import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ScrollContext } from "react-router-scroll-4";
import { IntlReducer as Intl, IntlProvider } from "react-redux-multilingual";
import "./index.scss";

// Import custom components
import store from "./store";
import translations from "./constants/translations";
import { getAllProducts } from "./actions";

// Layouts
import Vegetables from "./components/layouts/vegetables/main";

//Collection Pages
import CollectionLeftSidebar from "./components/collection/collection-left-sidebar";

// Product Pages
import LeftSideBar from "./components/products/left-sidebar";

// Features
import Layout from "./components/app";
import Cart from "./components/cart";
import Compare from "./components/compare/index";
import wishList from "./components/wishlist";
import checkOut from "./components/checkout";
import Paysenz from "./components/paysenz/paysenz";
import orderSuccess from "./components/checkout/success-page";

// Extra Pages
import aboutUs from "./components/pages/about-us";
import Privacy from "./components/pages/privacy";
import PageNotFound from "./components/pages/404";
import Login from "./components/pages/login";
import Register from "./components/pages/register";
import ForgetPassword from "./components/pages/forget-password";
import Contact from "./components/pages/contact";
import Dashboard from "./components/pages/dashboard";
import ThankYou from "./components/pages/thank-you";
import ChangePass from "./components/pages/change-pass";
import MyOrders from "./components/pages/my-orders";

// Blog Pages
import RightSide from "./components/blogs/right-sidebar";
import Details from "./components/blogs/details";
import API from "./config";

let BASE_URL =`${API.api_root_url}`;
const API_URL = `${BASE_URL}/api/v1/e-commerce/shop/products-list`;

class Root extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allDataList: []
        };
    }

    componentDidMount() {
        this.getFullProductList();
    }

    getFullProductList() {
        fetch(API_URL)
            .then(res => res.json())
            .then(json => {
                console.log('TotalProducts Fetch');
                console.log(json);
                this.setState({
                    allDataList: json
                });
            });
    }

    render() {
        store.dispatch(getAllProducts(this.state.allDataList));

        return (
            <Provider store={store}>
                <IntlProvider translations={translations} locale="en">
                    <BrowserRouter basename={"/"}>
                        <ScrollContext>
                            <Switch>
                                <Route exact path={`${process.env.PUBLIC_URL}/`} component={Vegetables} />
                                {/*<Route path={`${process.env.PUBLIC_URL}/vegetables`} component={Vegetables}/>*/}
                                <Layout>
                                    {/*Routes For Features (Product Collection)   Url should Not be changeable last if url change then component will be change*/}
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/shop`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/nursery`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/baby-item`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/personal-care`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/undergarments`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/makup-&-beauty`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/shoes`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/perfume-&-mist`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/jewellery`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/toys`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/collection/giftitem`} component={CollectionLeftSidebar} />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/left-sidebar/collection/woman-&-men-accessories`}
                                        component={CollectionLeftSidebar}
                                    />

                                    {/*Routes For Category*/}
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/baby-accessories/collection/bedding-accessories`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route path={`${process.env.PUBLIC_URL}/baby-accessories/collection/bottle-feeding`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/baby-accessories/collection/newborn-babies`} component={CollectionLeftSidebar} />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/baby-accessories/collection/soothers-pacifier-teethers`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route path={`${process.env.PUBLIC_URL}/baby-accessories/collection/baby-carrier`} component={CollectionLeftSidebar} />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/baby-accessories/collection/feeding-accessories`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route path={`${process.env.PUBLIC_URL}/baby-accessories/collection/maternity-bag`} component={CollectionLeftSidebar} />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/baby-accessories/collection/bathing-accessories`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/baby-accessories/collection/flask-lunchbox-containers`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/baby-accessories/collection/diapers-and-changing-accessories`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/baby-accessories/collection/baby-personal-care`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/baby-accessories/collection/newborn-accessories`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route path={`${process.env.PUBLIC_URL}/baby-accessories/collection/wardrobes`} component={CollectionLeftSidebar} />

                                    {/*Routes For Category*/}
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/cot-and-crib`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/pushchairs-and-strollers`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/baby-vehicles`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/rockers`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/bouncing-cradles`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/bicycles-and-scooters`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/swings`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/walkers`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/push-can`} component={CollectionLeftSidebar} />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/nursery/collection/high-chairs-and-booster-seats`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/car-seats`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/nursery/collection/jumperoo`} component={CollectionLeftSidebar} />

                                    {/*Routes For Category*/}
                                    <Route path={`${process.env.PUBLIC_URL}/baby-0-18/collection/sets-in-all-ones`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/baby-0-18/collection/newborn-collection`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/baby-0-18/collection/tops-and-bottoms`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/baby-0-18/collection/romper`} component={CollectionLeftSidebar} />

                                    {/*Routes For Category*/}
                                    <Route path={`${process.env.PUBLIC_URL}/girls-clothing/collection/kurti-and-kamez`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/girls-clothing/collection/frocks-and-skirts`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/girls-clothing/collection/lehenga`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/girls-clothing/collection/ghara`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/girls-clothing/collection/gown`} component={CollectionLeftSidebar} />

                                    <Route path={`${process.env.PUBLIC_URL}/boys-clothing/collection/top-and-t-shirts`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/boys-clothing/collection/suits-and-jackets`} component={CollectionLeftSidebar} />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/boys-clothing/collection/jeans-tousers-and-shorts`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route
                                        path={`${process.env.PUBLIC_URL}/boys-clothing/collection/kurtas-and-sherwani`}
                                        component={CollectionLeftSidebar}
                                    />
                                    <Route path={`${process.env.PUBLIC_URL}/boys-clothing/collection/sets-and-outfits`} component={CollectionLeftSidebar} />
                                    <Route path={`${process.env.PUBLIC_URL}/boys-clothing/collection/shirts`} component={CollectionLeftSidebar} />

                                    {/*Routes For Single Product*/}
                                    <Route path={`${process.env.PUBLIC_URL}/left-sidebar/product/:id`} component={LeftSideBar} />

                                    {/*Routes For custom Features*/}
                                    <Route path={`${process.env.PUBLIC_URL}/cart`} component={Cart} />
                                    <Route path={`${process.env.PUBLIC_URL}/wishlist`} component={wishList} />
                                    <Route path={`${process.env.PUBLIC_URL}/compare`} component={Compare} />
                                    <Route path={`${process.env.PUBLIC_URL}/checkout`} component={checkOut} />
                                    <Route path={`${process.env.PUBLIC_URL}/paysenz-order`} component={Paysenz} />
                                    <Route path={`${process.env.PUBLIC_URL}/order-success`} component={orderSuccess} />

                                    <Route path={`${process.env.PUBLIC_URL}/sales/orders`} component={aboutUs} />

                                    {/*Routes For Extra Pages*/}
                                    <Route path={`${process.env.PUBLIC_URL}/pages/about-us`} component={aboutUs} />
                                    <Route path={`${process.env.PUBLIC_URL}/pages/privacy`} component={Privacy} />
                                    <Route path={`${process.env.PUBLIC_URL}/pages/404`} component={PageNotFound} />
                                    <Route path={`${process.env.PUBLIC_URL}/login`} component={Login} />
                                    <Route path={`${process.env.PUBLIC_URL}/register`} component={Register} />
                                    <Route path={`${process.env.PUBLIC_URL}/pages/forget-password`} component={ForgetPassword} />
                                    <Route path={`${process.env.PUBLIC_URL}/pages/contact`} component={Contact} />
                                    <Route path={`${process.env.PUBLIC_URL}/dashboard`} component={Dashboard} />
                                    <Route path={`${process.env.PUBLIC_URL}/pages/thank-you`} component={ThankYou} />
                                    <Route path={`${process.env.PUBLIC_URL}/change-pass`} component={ChangePass} />
                                    <Route path={`${process.env.PUBLIC_URL}/my-orders`} component={MyOrders} />

                                    {/*Blog Pages*/}
                                    <Route path={`${process.env.PUBLIC_URL}/blog/right-sidebar`} component={RightSide} />
                                    <Route path={`${process.env.PUBLIC_URL}/blog/details`} component={Details} />

                                    {/* <Route exact path="*" component={PageNotFound} /> */}
                                </Layout>
                            </Switch>
                        </ScrollContext>
                    </BrowserRouter>
                </IntlProvider>
            </Provider>
        );
    }
}

ReactDOM.render(<Root />, document.getElementById("root"));
