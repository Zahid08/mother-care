// Get Unique Brands from Json Data
export const getBrands = (products,category) => {
    let product_items;

    if (category=='shop'){
        var uniqueBrands = [];
        products.map((product, index) => {
            if (product.tags) {
                product.tags.map((tag) => {
                    if (uniqueBrands.indexOf(tag) === -1) {
                        uniqueBrands.push(tag);
                    }
                })
            }
        })
        return uniqueBrands;
    }else {
        product_items = products.filter(product => {
            for(var i = 0; i <  product.main_category.length; i++) {
                if (product.main_category[i]== category){
                    return product;
                }
            }
        });

        var uniqueBrands = [];
        product_items.map((product, index) => {
            if (product.tags) {
                product.tags.map((tag) => {
                    if (uniqueBrands.indexOf(tag) === -1) {
                        uniqueBrands.push(tag);
                    }
                })
            }
        })
        //console.log(uniqueBrands)
        return uniqueBrands;
    }
}

// Get Unique Colors from Json Data
export const getColors = (products,category) => {

    let product_items;

    if (category=='shop'){
        product_items=products;
    }else {
        product_items = products.filter(product => {
            for(var i = 0; i <  product.main_category.length; i++) {
                if (product.main_category[i]== category){
                    return product;
                }
            }
        });
    }

    var uniqueColors = [];
    product_items.map((product, index) => {
        if(product.colors) {
            product.colors.map((color) => {
                if (uniqueColors.indexOf(color) === -1) {
                    uniqueColors.push(color);
                }
            })
        }
    })
    //console.log(uniqueBrands)
    return uniqueColors;
}

// Get Minimum and Maximum Prices from Json Data
export const getMinMaxPrice = (products,category) => {
    let min = 100, max = 1000;

    let product_items;

    if (category=='shop'){
        product_items=products;
    }else {
        product_items = products.filter(product => {
            for(var i = 0; i <  product.main_category.length; i++) {
                if (product.main_category[i]== category){
                    return product;
                }
            }
        });
    }

    product_items.map((product, index) => {
        let v = product.price;
        min = (v < min) ? v : min;
        max = (v > max) ? v : max;
    })

    return {'min':min, 'max':max};
}


export const getMatchproducts = (data, { brand, color, value, sortBy },category) => {
    let product_items;
    if (category=='shop'){
        return data.products.sort((product1, product2) => {
            if (sortBy === 'HighToLow') {
                return product2.price < product1.price ? -1 : 1;
            } else if (sortBy === 'LowToHigh') {
                return product2.price > product1.price ? -1 : 1;
            } else if (sortBy === 'Newest') {
                return product2.id < product1.id ? -1 : 1;
            } else if (sortBy === 'AscOrder') {
                return product1.name.localeCompare(product2.name);
            } else if (sortBy === 'DescOrder') {
                return product2.name.localeCompare(product1.name);
            } else{
                return product2.id > product1.id ? -1 : 1;
            }
        });
    }else {
        product_items = data.products.filter(product => {
            for(var i = 0; i <  product.main_category.length; i++) {
                if (product.main_category[i]== category){
                    return product;
                }
            }
        });
        return product_items.sort((product1, product2) => {
            if (sortBy === 'HighToLow') {
                return product2.price < product1.price ? -1 : 1;
            } else if (sortBy === 'LowToHigh') {
                return product2.price > product1.price ? -1 : 1;
            } else if (sortBy === 'Newest') {
                return product2.id < product1.id ? -1 : 1;
            } else if (sortBy === 'AscOrder') {
                return product1.name.localeCompare(product2.name);
            } else if (sortBy === 'DescOrder') {
                return product2.name.localeCompare(product1.name);
            } else{
                return product2.id > product1.id ? -1 : 1;
            }
        });
    }
}

export const getVisibleproducts = (data, { brand, color, value, sortBy },category) => {
        let product_items;
       if (category=='shop'){
           return data.products.filter(product => {

               let brandMatch;
               if(product.tags)
                   brandMatch = product.tags.some(tag => brand.includes(tag))
               else
                   brandMatch = true;

               let colorMatch;
               if(color && product.colors) {
                   colorMatch = product.colors.includes(color)
               }else{
                   colorMatch = true;
               }

               const startPriceMatch = typeof value.min !== 'number' || value.min <= product.price;
               const endPriceMatch = typeof value.max !== 'number' || product.price <= value.max;

               return brandMatch && colorMatch && startPriceMatch && endPriceMatch;
           }).sort((product1, product2) => {
               if (sortBy === 'HighToLow') {
                   return product2.price < product1.price ? -1 : 1;
               } else if (sortBy === 'LowToHigh') {
                   return product2.price > product1.price ? -1 : 1;
               } else if (sortBy === 'Newest') {
                   return product2.id < product1.id ? -1 : 1;
               } else if (sortBy === 'AscOrder') {
                   return product1.name.localeCompare(product2.name);
               } else if (sortBy === 'DescOrder') {
                   return product2.name.localeCompare(product1.name);
               } else{
                   return product2.id > product1.id ? -1 : 1;
               }
           });
       }else {
            product_items = data.products.filter(product => {
               for(var i = 0; i <  product.main_category.length; i++) {
                  if (product.main_category[i]== category){
                      return product;
                  }
               }
           });
           return product_items.filter(product => {

               let brandMatch;
               if(product.tags)
                   brandMatch = product.tags.some(tag => brand.includes(tag))
               else
                   brandMatch = true;

               let colorMatch;
               if(color && product.colors) {
                   colorMatch = product.colors.includes(color)
               }else{
                   colorMatch = true;
               }

               const startPriceMatch = typeof value.min !== 'number' || value.min <= product.price;
               const endPriceMatch = typeof value.max !== 'number' || product.price <= value.max;

               return brandMatch && colorMatch && startPriceMatch && endPriceMatch;
           }).sort((product1, product2) => {
               if (sortBy === 'HighToLow') {
                   return product2.price < product1.price ? -1 : 1;
               } else if (sortBy === 'LowToHigh') {
                   return product2.price > product1.price ? -1 : 1;
               } else if (sortBy === 'Newest') {
                   return product2.id < product1.id ? -1 : 1;
               } else if (sortBy === 'AscOrder') {
                   return product1.name.localeCompare(product2.name);
               } else if (sortBy === 'DescOrder') {
                   return product2.name.localeCompare(product1.name);
               } else{
                   return product2.id > product1.id ? -1 : 1;
               }
           });
       }
}

export const getCartTotal = cartItems => {
    var total = 0;
    for(var i=0; i<cartItems.length; i++){
        // total += parseInt(cartItems[i].qty, 10)*parseInt((cartItems[i].price*cartItems[i].discount/100), 10);
        total += parseInt(cartItems[i].qty, 10)*parseInt((cartItems[i].salePrice), 10);
    }
    return total;
}

// Get Trending Tag wise Collection
export const getTrendingTagCollection = (products, type, tag) => {
    const items = products.filter(product => {
        return product.category === type && product.tags.includes(tag);
    })
    return items.slice(0,8)
}

// Get Trending Collection
export const getTrendingCollection = (products, type) => {
    const items = products.filter(product => {
        return product.category === type;
    })
    return items.slice(0,8)
}

// Get Special 5 Collection
export const getSpecialCollection = (products, type) => {
    const items = products.filter(product => {
        return product.category === type;
    })
    return items.slice(0,5)
}

// Get TOP Collection
export const getTopCollection = products => {
    const items = products.filter(product => {
        return product.rating > 4;
    })
    return items.slice(0,8)
}

// Get New Products
export const getNewProducts = (products, type) => {
    const items = products.filter(product => {
        return product.new === true && product.category === type;
    })

    return items.slice(0,8)
}

// Get Related Items
export const getRelatedItems = (products, type) => {
    const items = products.filter(product => {
        return product.category === type;
    })

    return items.slice(0,4)

}

// Get Best Seller Furniture
export const getBestSellerProducts = (products, type) => {
    const items = products.filter(product => {
        return product.sale === true && product.category === type;
    })

    return items.slice(0,8)
}

// Get Best Seller
export const getBestSeller = (products,category) => {
    let product_items;
    if (category=='shop'){
        product_items=products;
    }else {
        product_items = products.filter(product => {
            for(var i = 0; i <  product.main_category.length; i++) {
                if (product.main_category[i]== category){
                    return product;
                }
            }
        });
    }

    const items = product_items.filter(product => {
        return product.sale === true;
    })

    return items.slice(0,8)
}
//getBestSellerNewProductDetails
export const getBestSellerNewProductDetails = (products) => {
    const items = products.filter(product => {
        return product.sale === true;
    })

    return items.slice(0,8)
}

// Get Mens Wear
export const getMensWear = products => {
    const items = products.filter(product => {
        return product.category === 'men';
    })

    return items.slice(0,8)
}

// Get Womens Wear
export const getWomensWear = products => {
    const items = products.filter(product => {
        return product.category === 'women';
    })

    return items.slice(0,8)
}

// Get Single Product
export const getSingleItem = (products, id) => {

    const items = products.find((element) => {
        return element.id === id;
    })
    return items;
}

//userPostFetch

export const userPostFetch = user => {

}

const loginUser = userObj => ({
    type: 'LOGIN_USER',
    payload: userObj
})

const initialState = {
    currentUser: {}
}

export function reducer(state = initialState, action) {
    switch (action.type) {
        case 'LOGIN_USER':
            return {state, currentUser: action.payload}
        default:
            return state;
    }
}
