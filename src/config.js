let base_url = "https://backenddokander.retalizer.com/api/v1";
let gateway_base_url=`https://sandbox.paysenz.com`;
var ApiInformation = {
      api_root_url: `https://backenddokander.retalizer.com`,
      site_url: `https://onlinedokander.retalizer.com`,
      register: `${base_url}/auth/register`,
      login: `${base_url}/auth/login`,
      profile: `${base_url}/profile`,
      rating: `${base_url}/e-commerce/shop/rating`,
      orders_submit: `${base_url}/e-commerce/checkout/order-submit`,
      success_order: `${base_url}/e-commerce/shop/order-success`,
      gateway:{
            basurl:`${gateway_base_url}`,
            authinticate:`${gateway_base_url}/oauth/token`,
            payment:`https://sandbox.paysenz.com/api/v1.0/pay`,
            client_id:`16`,
            client_secret:`2XeYHZSAorBmqowLmCOUXFWLTb21kGRlekEfCe46`,
            username:`info@unlocklive.com`,
            password:`123456`,
      }
};

export default ApiInformation;
