import React, { Component } from 'react';
import Slider from 'react-slick';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import API from "../../../config";

import {getSingleItem, getSpecialCollection} from '../../../services/index'
import {
    addToCart,
    addToWishlist,
    addToCompare,
    incrementQty,
    decrementQty,
    removeFromCart
} from "../../../actions/index";
import TopSellItem from './TopSell-item';
import {Product4} from "../../../services/script";
import ProductItem from "../common/special-product-item";

let BASE_URL = `${API.api_root_url}`;
const API_URL = `${BASE_URL}/api/v1/e-commerce/shop/products-top-sell-weekly`;

class TopSell extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemsTopSell: []
        }
    }

    componentDidMount() {
        this.getTopSellContent();
    }

    getTopSellContent() {
        fetch(API_URL)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    itemsTopSell: json
                })
            });
    }

    render (){
        var { itemsTopSell } = this.state;
        const {symbol, addToCart, addToWishlist, addToCompare, incrementQty, decrementQty, removeFromCart} = this.props;
        return (
            <div>
                {/*Paragraph*/}
                <section className="section-b-space addtocart_count" style={{paddingTop: "0"}}>
                    <div className="full-box">
                        <div className="container">
                            <div className="title4">
                                <h2 className="title-inner4">Top Selling of the week</h2>
                                <h2 className="view-all" style={{float: "right"}}><a href="#">View All</a></h2>
                                <div className="line"><span></span></div>
                            </div>
                            <div className="row">
                                { itemsTopSell.map((product, index ) =>
                                    <div  key={index} className="col-md-3 center-slider">
                                        <div class="product-item">
                                            <div className="offer-slider">
                                                <div>
                                                    <ProductItem product={product} symbol={symbol}
                                                                 onAddToCompareClicked={() => addToCompare(product)}
                                                                 onAddToWishlistClicked={() => addToWishlist(product)}
                                                                 onAddToCartClicked={() => addToCart(product, 1)}
                                                                 onIncrementClicked={() => incrementQty(product, 1)}
                                                                 onDecrementClicked={() => decrementQty(product.id)}
                                                                 onRemoveFromCart={() => removeFromCart(product)}
                                                                 key={index} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>)
                                }
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state, Ownprops) => ({
    product: getSpecialCollection(state.data.products, Ownprops.type),
    symbol: state.data.symbol
})

export default connect(mapStateToProps,
    {
        addToCart,
        addToWishlist,
        addToCompare,
        incrementQty,
        decrementQty,
        removeFromCart
    }) (TopSell);