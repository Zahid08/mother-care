import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {connect} from 'react-redux';
import API from "../../../config";

import {getBestSeller, getMensWear, getWomensWear} from '../../../services/index'
import {addToCart, addToWishlist, addToCompare} from "../../../actions/index";
import ProductItem from './product-item';

let BASE_URL = `${API.api_root_url}`;
const API_URL_TOP_SELL = `${BASE_URL}/api/v1/e-commerce/shop/products-top-sell`;
const API_URL_MOST_LIKE = `${BASE_URL}/api/v1/e-commerce/shop/products-most-like`;
const API_URL_SPECIAL = `${BASE_URL}/api/v1/e-commerce/shop/products-special-product`;

class SpecialProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bestSeller: [],
            mostLike: [],
            specials: [],
        }
    }

    componentDidMount() {
        this.getContent();
        this.getMostLikeContent();
        this.getspecialContent();
    }

    getContent() {
        fetch(API_URL_TOP_SELL)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    bestSeller: json
                })
            });
    }
    getMostLikeContent() {
        fetch(API_URL_MOST_LIKE)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    mostLike: json
                })
            });
    }

    getspecialContent() {
        fetch(API_URL_SPECIAL)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    specials: json
                })
            });
    }

    render (){
        var { bestSeller ,mostLike,specials} = this.state;
        const {mensWear,womensWear, symbol, addToCart, addToWishlist, addToCompare} = this.props
        return (
            <div>
                <section className="section-b-space p-t-0" style={{paddingTop: "0"}}>
                    <div className="container">
                        <Tabs className="theme-tab">
                            <TabList className="tabs tab-title">
                                <Tab>Top Sales</Tab>
                                <Tab>Special</Tab>
                                <Tab>Most Like</Tab>
                                <h2 style={{float: "right", fontSize: "16px" }}><a href="#">View All</a></h2>
                            </TabList>


                            <TabPanel>
                                <div className="no-slider row">
                                    { bestSeller.map((product, index ) =>
                                        <ProductItem product={product} symbol={symbol}
                                                     onAddToCompareClicked={() => addToCompare(product)}
                                                     onAddToWishlistClicked={() => addToWishlist(product)}
                                                     onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                    }
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className="no-slider row">
                                    { specials.map((product, index ) =>
                                        <ProductItem product={product} symbol={symbol}
                                                     onAddToCompareClicked={() => addToCompare(product)}
                                                     onAddToWishlistClicked={() => addToWishlist(product)}
                                                     onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                    }
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className=" no-slider row">
                                    { mostLike.map((product, index ) =>
                                        <ProductItem product={product} symbol={symbol}
                                                     onAddToCompareClicked={() => addToCompare(product)}
                                                     onAddToWishlistClicked={() => addToWishlist(product)}
                                                     onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                    }
                                </div>
                            </TabPanel>
                        </Tabs>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    bestSeller: getBestSeller(state.data.products),
    mensWear: getMensWear(state.data.products),
    womensWear: getWomensWear(state.data.products),
    symbol: state.data.symbol
})

export default connect(mapStateToProps, {addToCart, addToWishlist, addToCompare}) (SpecialProducts);