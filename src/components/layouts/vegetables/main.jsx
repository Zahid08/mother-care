import React, { Component } from 'react';
import {Helmet} from 'react-helmet'
import '../../common/index.scss';
import Slider from 'react-slick';
import {Link} from 'react-router-dom';


// Import custom components
import {Slider3} from "../../../services/script"
import Trading from "./tranding"
import Special from "./special"
import TopSell from "./TopSell"
import {
    svgFreeShipping,
    svgservice,
    svgoffer
} from "../../../services/script"
import HeaderTwo from "../../common/headers/header-two"
import FooterOne from "../../common/footers/footer-one"
import ThemeSettings from "../../common/theme-settings"
import SpecialProducts from "./products";
import API from "../../../config";


let BASE_URL = `${API.api_root_url}`;
const API_URL = `${BASE_URL}/api/v1/e-commerce/shop/products-list`;

class Vegetables extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        }
    }

    componentDidMount() {
        document.getElementById("color").setAttribute("href", `#` );
        this.getAllProducts();
    }
    getAllProducts() {
        fetch(API_URL)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    products: json
                })
            });
    }

    render(){
        return (
            <div style={{backgroundColor:'#f9f9f9',backgroundSize: "contain",backgroundPosition: "center center"}}>
                <Helmet>
                    <title>Welcome | UnlockLive IT Limited</title>
                </Helmet>
                <HeaderTwo logoName={'logo.png'} />
                <section className="p-0">
                    <Slider className="slide-1 home-slider">
                        <div>
                            <div className="home home39 text-center">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col">
                                                <div className="slider-contain">
                                                    <div>
                                                        <h4>save 10%</h4>
                                                        <h1>New Clothing</h1><a href={`${process.env.PUBLIC_URL}/left-sidebar/collection/shop`} className="btn btn-solid">shop
                                                        now</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div>
                            <div className="home home38 text-center">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col">
                                                <div className="slider-contain">
                                                    <div>
                                                        <h4>save upto 10%</h4>
                                                        <h1>fresh vegetables</h1><a href={`${process.env.PUBLIC_URL}/left-sidebar/collection/shop`} className="btn btn-solid">shop
                                                        now</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </Slider>
                </section>

                {/*product section Start*/}
                <Trading  type={'vegetable'} product={this.state.products}/>
                {/*product section End*/}

                <SpecialProducts product={this.state.products} />

                {/*product-box slider*/}
                <TopSell type={'vegetable'} product={this.state.products} />
                {/*product-box slider end*/}

                {/*product-box slider*/}
                <Special type={'vegetable'} product={this.state.products}/>
                {/*product-box slider end*/}

                {/*Blog Section*/}
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="title4">
                                <h2 className="title-inner4">from our news</h2>
                                <div className="line"><span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="blog section-b-space pt-0 ratio2_3">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <Slider {...Slider3} className="slide-3 no-arrow">
                                    <div className="col-md-12">
                                        <Link to={`${process.env.PUBLIC_URL}/blog/details`} >
                                            <div className="classic-effect">
                                                <div>
                                                    <img src={`${process.env.PUBLIC_URL}/assets/images/vegetables/blog/1.jpg`}
                                                         className="img-fluid blur-up lazyload bg-img" alt="" />
                                                </div>
                                                <span></span>
                                            </div>
                                        </Link>
                                        <div className="blog-details">
                                            <h4>25 January 2018</h4>
                                            <a href="#">
                                                <p>Lorem ipsum dolor sit consectetur adipiscing elit,</p>
                                            </a>
                                            <hr className="style1" />
                                                <h6>by: John Dio , 2 Comment</h6>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <a href="#">
                                            <div className="classic-effect">
                                                <div>
                                                    <img src={`${process.env.PUBLIC_URL}/assets/images/vegetables/blog/2.jpg`}
                                                         className="img-fluid blur-up lazyload bg-img" alt="" />
                                                </div>
                                                <span></span>
                                            </div>
                                        </a>
                                        <div className="blog-details">
                                            <h4>25 January 2018</h4>
                                            <a href="#">
                                                <p>Lorem ipsum dolor sit consectetur adipiscing elit,</p>
                                            </a>
                                            <hr className="style1" />
                                                <h6>by: John Dio , 2 Comment</h6>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <a href="#">
                                            <div className="classic-effect">
                                                <div>
                                                    <img src={`${process.env.PUBLIC_URL}/assets/images/vegetables/blog/3.jpg`}
                                                         className="img-fluid blur-up lazyload bg-img" alt="" />
                                                </div>
                                                <span></span>
                                            </div>
                                        </a>
                                        <div className="blog-details">
                                            <h4>25 January 2018</h4>
                                            <a href="#">
                                                <p>Lorem ipsum dolor sit consectetur adipiscing elit,</p>
                                            </a>
                                            <hr className="style1" />
                                                <h6>by: John Dio , 2 Comment</h6>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <a href="#">
                                            <div className="classic-effect">
                                                <div>
                                                    <img src={`${process.env.PUBLIC_URL}/assets/images/vegetables/blog/4.jpg`}
                                                         className="img-fluid blur-up lazyload bg-img" alt="" />
                                                </div>
                                                <span></span>
                                            </div>
                                        </a>
                                        <div className="blog-details">
                                            <h4>25 January 2018</h4>
                                            <a href="#">
                                                <p>Lorem ipsum dolor sit consectetur adipiscing elit,</p>
                                            </a>
                                            <hr className="style1" />
                                                <h6>by: John Dio , 2 Comment</h6>
                                        </div>
                                    </div>
                                </Slider>
                            </div>
                        </div>
                    </div>
                </section>
                {/*Blog Section End*/}
                <ThemeSettings/>
                <FooterOne logoName={'logo.png'} />
            </div>
        )
    }
}


export default Vegetables