import React, {Component} from 'react';
import {Helmet} from 'react-helmet'
import { connect } from 'react-redux'
import {Link, Redirect } from 'react-router-dom'
import SimpleReactValidator from 'simple-react-validator';
import Breadcrumb from "../common/breadcrumb";
import {removeFromWishlist} from '../../actions'
import {getCartTotal} from "../../services";
import Paysenz from "../paysenz/paysenz";
import API from "../../config"

class checkOut extends Component {

    constructor (props) {
        super (props)

        this.state = {
            payment:'stripe',
            first_name:'',
            last_name:'',
            phone:'',
            email:'',
            country:'',
            address:'',
            city:'',
            state:'',
            pincode:'',
            create_account: '',
            showClickButoon: true,
            order_id:'',
            order_number:'',
            billing_address:'',
            user_id:''
        }
        this.validator = new SimpleReactValidator();
        this.PaysenzClick = this.PaysenzClick.bind(this);
    }

    setStateFromInput = (event) => {
        var obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);

      }

      setStateFromCheckbox = (event) => {
          var obj = {};
          obj[event.target.name] = event.target.checked;
          this.setState(obj);

          if(!this.validator.fieldValid(event.target.name))
          {
              this.validator.showMessages();
          }
        }

    checkhandle(value) {
        this.setState({
            payment: value
        })
    }

    CashOnDeliveryClick = () => {
        if (this.validator.allValid()) {
           alert('You submitted the form and stuff!');
           this.SaveBillingOrder('CashOnDelivery','Cash');
           this.props.history.push({
                pathname: '/order-success',
                state: { payment: 'payment30101', items: this.props.cartItems, orderTotal: this.props.total, symbol: this.props.symbol }
            })
        } else {
          this.validator.showMessages();
          // rerender to show messages for the first time
          this.forceUpdate();
        }
    }

    //paysenz button click and check validation
    PaysenzClick=()=>{
        if (this.validator.allValid()) {
            this.SaveBillingOrder('Paysenz','OnlinePayment');
        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }
    }

    //Billing Information Save And Pass Data TO Paysenz
    SaveBillingOrder=(payment_reference,gateway)=>{
        console.log(this.props.cartItems);
        if (this.props.cartItems){
            var cartItemList = this.props.cartItems;
            var order_items = cartItemList.map(function (cartItemData, i) {
                return {
                    "amount": cartItemData.sum?cartItemData.sum:0,
                    "quantity":cartItemData.qty?cartItemData.qty:'',
                    "type": cartItemData.category?cartItemData.category:'NullType',
                    "sku_code":cartItemData.squ_code?cartItemData.squ_code:'',
                    "description": cartItemData.description?cartItemData.description:'blank description',
                    "tax_ids": [],
                    "properties": {
                        "sku_id":  cartItemData.id?cartItemData.id:''
                    },
                    "item_options": null
                };
            });

            var itemStore = cartItemList.map(function (cartItemData, i) {
                return {
                    "itemname": cartItemData.name?cartItemData.name:'',
                    "price":cartItemData.salePrice?cartItemData.salePrice:'',
                    "color": cartItemData.color?cartItemData.color:'',
                    "size": cartItemData.size?cartItemData.size:'',
                    "images":cartItemData.images?cartItemData.images:cartItemData.pictures[0],
                    "quantity": cartItemData.qty?cartItemData.qty:'',
                };
            });

            let order_details={
                "amount": this.props.total,
                "currency": "BDT",
                "save_billing": true,
                "save_shipping": true,
                "status": "submitted",
                "payment_reference":payment_reference,
                "gateway":gateway,
                "payment_status": "pending",
                "billing_address": {
                    "first_name": this.state.first_name?this.state.first_name:'',
                    "last_name":  this.state.last_name?this.state.last_name:'',
                    "email": this.state.email?this.state.email:'',
                    "address_1": this.state.address?this.state.address:'',
                    "address_2": "",
                    "city": this.state.city?this.state.city:'',
                    "state": this.state.state?this.state.state:'',
                    "country":  this.state.country?this.state.country:'',
                    "zip": this.state.pincode?this.state.pincode:'',
                    "itemStore":itemStore,
                    "image": this.props.cartItems[0].images? this.props.cartItems[0].images  : this.props.cartItems[0].pictures[0] ? this.props.cartItems[0].pictures[0]:``,
                },
                "shipping_address": {
                    "first_name": this.state.first_name?this.state.first_name:'',
                    "last_name":  this.state.last_name?this.state.last_name:'',
                    "email": this.state.email?this.state.email:'',
                    "address_1": this.state.address?this.state.address:'',
                    "address_2": "",
                    "city": this.state.city?this.state.city:'',
                    "state": this.state.state?this.state.state:'',
                    "country":  this.state.country?this.state.country:'',
                    "zip": this.state.pincode?this.state.pincode:'',
                },
                "order_items":order_items
            };
            var token = localStorage.getItem('token');

            fetch(`${API.orders_submit}`, {
                method: "post",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization': `${token}`
                },
                body: JSON.stringify(order_details)
            }).then(resp => resp.json())
                .then(order_response => {
                    console.log("order_sucess_response_data");
                    if (order_response.status=="success"){
                        this.setState({
                            order_id:order_response.data.id
                        });
                        this.setState({
                            order_number:order_response.data.order_number
                        }); //billing_address
                        this.setState({
                            billing_address:order_response.data.billing
                        });
                        this.setState({
                            user_id:order_response.data.user_id
                        });
                        this.setState({ showClickButoon: !this.state.showClickButoon })  //Open Paysenz Button
                    }
                })

        }
    }

    render (){
        //Session Destroy
        var tokenCheck = localStorage.getItem('token');
        if (tokenCheck){
        }else {
            this.props.history.push("/login");
        }
        const {cartItems, symbol, total,translate} = this.props;

        console.log('RRRRRRRRRRRRRRRRRRRRRRRRRRRRR');
        console.log(cartItems);
        return (
            <div>
                {/*SEO Support*/}
                <Helmet>
                    <title>MotherCare | CheckOut Page</title>
                    <meta name="description" content="Multikart – Multipurpose eCommerce React Template is a multi-use React template. It is designed to go well with multi-purpose websites. Multikart Bootstrap 4 Template will help you run multiple businesses." />
                </Helmet>
                {/*SEO Support End */}
                <Breadcrumb  title={'Checkout'}/>

                <section className="section-b-space">
                    <div className="container padding-cls">
                        <div className="checkout-page">
                            <div className="checkout-form">
                                <form>
                                    <div className="checkout row">
                                        <div className="col-lg-6 col-sm-12 col-xs-12">
                                            <div className="checkout-title">
                                                <h3>Billing Address</h3>
                                            </div>
                                            <div className="row check-out">
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <div className="field-label">First Name</div>
                                                    <input type="text" name="first_name" value={this.state.first_name} onChange={this.setStateFromInput} />
                                                    {this.validator.message('first_name', this.state.first_name, 'required|alpha')}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <div className="field-label">Last Name</div>
                                                    <input type="text" name="last_name" value={this.state.last_name} onChange={this.setStateFromInput} />
                                                    {this.validator.message('last_name', this.state.last_name, 'required|alpha')}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <div className="field-label">Phone</div>
                                                    <input type="text" name="phone"  value={this.state.phone} onChange={this.setStateFromInput} />
                                                    {this.validator.message('phone', this.state.phone, 'required|phone')}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <div className="field-label">Email Address</div>
                                                    <input type="text" name="email" value={this.state.email} onChange={this.setStateFromInput} />
                                                    {this.validator.message('email', this.state.email, 'required|email')}
                                                </div>
                                                <div className="form-group col-md-12 col-sm-12 col-xs-12">
                                                    <div className="field-label">Country</div>
                                                    <select name="country" value={this.state.country} onChange={this.setStateFromInput}>
                                                        <option value="Australia">Australia</option>
                                                        <option value="Afghanistan">Afghanistan</option>
                                                        <option value="Albania">Albania</option>
                                                        <option value="Bangladesh">Bangladesh</option>
                                                        <option value="India">India</option>
                                                        <option value="SouthAfrica">South Africa</option>
                                                        <option value="UnitedState">United State</option>
                                                    </select>
                                                    {this.validator.message('country', this.state.country, 'required')}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-12 col-xs-12">
                                                    <div className="field-label">Address</div>
                                                    <input type="text" name="address" value={this.state.address} onChange={this.setStateFromInput} placeholder="Street address" />
                                                    {this.validator.message('address', this.state.address, 'required|min:20|max:120')}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-12 col-xs-12">
                                                    <div className="field-label">Town/City</div>
                                                    <input type="text" name="city" value={this.state.city} onChange={this.setStateFromInput} />
                                                    {this.validator.message('city', this.state.city, 'required|alpha')}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <div className="field-label">State / County</div>
                                                    <input type="text" name="state" value={this.state.state} onChange={this.setStateFromInput} />
                                                    {this.validator.message('state', this.state.state, 'required|alpha')}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <div className="field-label">Postal Code</div>
                                                    <input type="text" name="pincode" value={this.state.spincode} onChange={this.setStateFromInput} />
                                                    {this.validator.message('pincode', this.state.pincode, 'required|integer')}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 col-sm-12 col-xs-12">
                                            <div className="checkout-details">
                                                <div className="order-box">
                                                    <div className="title-box">
                                                        <div>Product <span> Total</span></div>
                                                    </div>
                                                    <ul className="qty">
                                                        {cartItems.map((item, index) => {
                                                            return <li key={index}>
                                                                <li key={index}>
                                                                    <img
                                                                        className="checkout-image"
                                                                        src={item.images ? item.images : item.pictures?item.pictures[0]:``}
                                                                        alt=""
                                                                    />
                                                                    <span
                                                                        style={{
                                                                            width: "183px",
                                                                            fontSize: "14px",
                                                                            float: "none",
                                                                            display: "inline-block",
                                                                            lineHeight: "initial",
                                                                            verticalAlign: "middle"
                                                                        }}
                                                                    >
                                                                {item.name}
                                                              </span>{" "}
                                                                    ×{" "}
                                                                    <span
                                                                        style={{
                                                                            verticalAlign: "middle",
                                                                            float: "none",
                                                                            width: "auto",
                                                                            fontSize: "16px",
                                                                            marginRight: "15px"
                                                                        }}
                                                                    >
                                                                {item.qty}
                                                              </span>{" "}
                                                                    <span
                                                                        style={{
                                                                            fontSize: "16px",
                                                                            width: "auto",
                                                                            verticalAlign: "middle"
                                                                        }}
                                                                    >
                                                                {symbol} {item.sum}
                                                              </span>
                                                                </li>
                                                            </li> })
                                                        }
                                                    </ul>
                                                    <ul className="sub-total">
                                                        <li>Subtotal <span className="count">{symbol}{total}</span></li>
                                                        <li>Shipping <div className="shipping">
                                                            <div className="shopping-option">
                                                                <input type="checkbox" name="free-shipping" id="free-shipping" />
                                                                    <label htmlFor="free-shipping">Free Shipping</label>
                                                            </div>
                                                        </div>
                                                        </li>
                                                    </ul>

                                                    <ul className="total">
                                                        <li>Total <span className="count">{symbol}{total}</span></li>
                                                    </ul>
                                                </div>

                                                <div className="payment-box">
                                                    <div className="upper-box">
                                                        <div className="payment-options">
                                                            <ul>
                                                                <li>
                                                                    <div className="radio-option stripe">
                                                                        <input type="radio" name="payment-group" id="payment-2" defaultChecked={true} onClick={() => this.checkhandle('stripe')} />
                                                                        <label htmlFor="payment-2">Cash On Delivery</label>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="radio-option paypal">
                                                                        <input type="radio" name="payment-group" id="payment-1" onClick={() => this.checkhandle('paysenz')} />
                                                                            <label htmlFor="payment-1">Paysenz<span className="image"><img src={`${process.env.PUBLIC_URL}/assets/images/paypal.png`} alt=""/></span></label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    {(total !== 0)?
                                                        <div className="text-right">
                                                            {(this.state.payment === 'stripe')? <button type="button" className="btn-solid btn" onClick={() => this.CashOnDeliveryClick()} >Place Order</button>:
                                                                (this.state.showClickButoon)?<button type="button" className="btn-solid btn" onClick={() => this.PaysenzClick()} >Paysenz Order</button>:<Paysenz items={this.props.cartItems} orderTotal={this.props.total} symbol={this.props.symbol} billing={this.state}/>}
                                                        </div>
                                                    : ''}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    cartItems: state.cartList.cart,
    symbol: state.data.symbol,
    total: getCartTotal(state.cartList.cart)
})

export default connect(
    mapStateToProps,
    {removeFromWishlist}
)(checkOut)