import React, {Component} from 'react';
import {
    removeAllCartItems,
} from "../../actions";
import connect from "react-redux/es/connect/connect";

class orderSuccess extends Component {
    constructor (props) {
        super (props)
        this.props.removeAllCartItems(); //Remove All Cart Items According TO Variables Name
    }
    render (){
        return (

            <div>
                {removeAllCartItems}
                <section className="section-b-space light-layout">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="success-text">
                                    <i className="fa fa-check-circle" aria-hidden="true"></i>
                                    <h2>thank you</h2>
                                    <p>Payment Is Has Been Received Order Placed Successfully</p>
                                    <a href={`${process.env.PUBLIC_URL}/my-orders`} className="btn btn-success">Go to my
                                        orders</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
export default connect(null,
    {
        removeAllCartItems,
    }) (orderSuccess);