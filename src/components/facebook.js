import React, { Component } from "react";
import FacebookLogin from "react-facebook-login";

class Facebook extends Component {
  state = {
    isLoggedIn: false,
    userID: "",
    name: "",
    email: "",
    picture: ""
  };
  responseFacebook = response => {
    this.setState({
      isLoggedIn: true,
      userID: response.userID,
      name: response.name,
      email: response.email,
      picture: response.picture.data.url
    });

    fetch("http://backend.motherscarebd.com/api/v1/auth/register", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: JSON.stringify({
        name: this.state.name,
        last_name: this.state.name,
        email: this.state.email,
        password: "123456",
        password_confirmation: "123456",
        terms: 1
      })
    })
      .then(Response => Response.json())
      .then(result => {
        if (result.errors) {
          console.log("Error");
        } else {
          console.log("Success");
          console.log(result);
          localStorage.setItem("token", result.data.authorization);
          window.location.href = "/checkout";
        }
      });
  };

  componentClicked = () => console.log("Clicked");

  render() {
    let fbContent;
    let { userID, name, email, picture } = this.state;
    console.log(userID);
    if (this.state.isLoggedIn) {
      fbContent = "Success!!";
      localStorage.setItem("token", userID);
      localStorage.setItem("author_id", userID);
      localStorage.setItem("author_name", name);
      localStorage.setItem("author_email", email);
      localStorage.setItem("author_picture", picture);
      window.location.href = "/checkout";
    } else {
      fbContent = (
        <FacebookLogin
          appId="789438711540087"
          autoLoad={true}
          fields="name,email,picture"
          onClick={this.componentClicked}
          callback={this.responseFacebook}
        />
      );
    }
    return <div>{fbContent}</div>;
  }
}

export default Facebook;
