import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withTranslate } from "react-redux-multilingual";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faHome,
    faStore,
    faCubes,
    faTshirt,
    faAddressBook,
    faAssistiveListeningSystems,
    faBasketballBall,
    faNewspaper,
    faFileAlt
} from "@fortawesome/free-solid-svg-icons";

class NavBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navClose: { right: "0px" }
        };
    }

    componentWillMount() {
        if (window.innerWidth < 750) {
            this.setState({ navClose: { right: "-410px" } });
        }
        if (window.innerWidth < 1199) {
            this.setState({ navClose: { right: "-300px" } });
        }
    }

    openNav() {
        console.log("open");
        this.setState({ navClose: { right: "0px" } });
    }
    closeNav() {
        this.setState({ navClose: { right: "-410px" } });
    }

    onMouseEnterHandler() {
        if (window.innerWidth > 1199) {
            document.querySelector("#main-menu").classList.add("hover-unset");
        }
    }

    handleSubmenu = event => {
        if (event.target.classList.contains("sub-arrow")) return;

        if (event.target.nextElementSibling.classList.contains("opensubmenu")) event.target.nextElementSibling.classList.remove("opensubmenu");
        else {
            document.querySelectorAll(".nav-submenu").forEach(function(value) {
                value.classList.remove("opensubmenu");
            });
            document.querySelector(".mega-menu-container").classList.remove("opensubmenu");
            event.target.nextElementSibling.classList.add("opensubmenu");
        }
    };

    handleMegaSubmenu = event => {
        if (event.target.classList.contains("sub-arrow")) return;

        if (event.target.parentNode.nextElementSibling.classList.contains("opensubmegamenu"))
            event.target.parentNode.nextElementSibling.classList.remove("opensubmegamenu");
        else {
            document.querySelectorAll(".menu-content").forEach(function(value) {
                value.classList.remove("opensubmegamenu");
            });
            event.target.parentNode.nextElementSibling.classList.add("opensubmegamenu");
        }
    };

    render() {
        const { translate } = this.props;
        return (
            <div>
                <div className="main-navbar">
                    <div id="mainnav">
                        <div className="toggle-nav" onClick={this.openNav.bind(this)}>
                            <i className="fa fa-bars sidebar-bar"></i>
                        </div>
                        <ul className="nav-menu" style={this.state.navClose}>
                            <li className="back-btn" onClick={this.closeNav.bind(this)}>
                                <div className="mobile-back text-right">
                                    <span>Back</span>
                                    <i className="fa fa-angle-right pl-2" aria-hidden="true"></i>
                                </div>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link to="/" className="nav-link" onClick={e => this.handleSubmenu(e)}>
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/home.png`} alt="" />
                                    
                                    {translate("home")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/shop`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/shop.png`} alt="" />
                                   
                                    {translate("shop")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/nursery`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/nursery.png`} alt="" />
                                   
                                    {translate("nursery")}
                                    <span className="sub-arrow"></span>
                                </Link>
                                <ul className="nav-submenu">
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/cot-and-crib`}>{translate("cot_crib")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/pushchairs-and-strollers`}>
                                            {translate("pushchain_stroller")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/baby-vehicles`}>{translate("baby_vehicles")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/rockers`}>{translate("rockers")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/bouncing-cradles`}>{translate("bouncing_cradles")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/bicycles-and-scooters`}>{translate("bicycles_scooters")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/swings`}>{translate("swings")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/walkers`}>{translate("walkers")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/push-can`}>{translate("push_car")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/high-chairs-and-booster-seats`}>
                                            {translate("highchairs_boosterseats")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/car-seats`}>{translate("car_seats")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/nursery/collection/jumperoo`}>{translate("jumperoo")}</Link>
                                    </li>
                                </ul>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    //   to={`${process.env.PUBLIC_URL}/left-sidebar/collection/baby-item`}
                                    to="#"
                                    className="dropdown"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/baby-item.png`} alt="" />
                                    
                                    {translate("babyaccessories")}
                                    <span className="sub-arrow"></span>
                                </Link>
                                <ul className="nav-submenu">
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/bedding-accessories`}>
                                            {translate("bedding_accessories")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/bottle-feeding`}>{translate("bottle_feeding")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/newborn-babies`}>{translate("newborn_babies")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/soothers-pacifier-teethers`}>
                                            {translate("soothers_pacifiers_teethers")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/baby-carrier`}>{translate("baby_carrier")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/feeding-accessories`}>
                                            {translate("feeding_accessories")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/maternity-bag`}>{translate("maternity_bag")}</Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/bathing-accessories`}>
                                            {translate("bathing_accessories")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/flask-lunchbox-containers`}>
                                            {translate("flask_lunchbox_containers")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/diapers-and-changing-accessories`}>
                                            {translate("diapers_changing_accessories")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/baby-personal-care`}>
                                            {translate("baby_personal_care")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/newborn-accessories`}>
                                            {translate("newborn_accessories")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${process.env.PUBLIC_URL}/baby-accessories/collection/wardrobes`}>{translate("wardrobes")}</Link>
                                    </li>
                                </ul>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/personal-care`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/care.png`} className="navIcon" alt="" />
                                    
                                    {translate("personalcare")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/undergarments`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/pant.png`} className="navIcon" alt="" />
                                    
                                    {translate("undergarments")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}} className="mega-menu">
                                <Link to="#" className="dropdown" onClick={e => this.handleSubmenu(e)}>
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/clothing.png`} alt="" />
                                    
                                    {translate("clothing")}
                                    <span className="sub-arrow"></span>
                                </Link>
                                <div className="mega-menu-container">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col mega-box">
                                                <div className="link-section">
                                                    <div className="menu-title">
                                                        <h5 onClick={e => this.handleMegaSubmenu(e)}>
                                                            {translate("baby_elements")}
                                                            <span className="sub-arrow"></span>
                                                        </h5>
                                                    </div>
                                                    <div className="menu-content">
                                                        <ul>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/baby-0-18/collection/sets-in-all-ones`}>
                                                                    {translate("sets_in_all")}
                                                                </Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/baby-0-18/collection/newborn-collection`}>
                                                                    {translate("new_born")}
                                                                </Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/baby-0-18/collection/tops-and-bottoms`}>
                                                                    {translate("top_bottom")}
                                                                </Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/baby-0-18/collection/romper`}>{translate("romper")}</Link>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col mega-box">
                                                <div className="link-section">
                                                    <div className="menu-title">
                                                        <h5 onClick={e => this.handleMegaSubmenu(e)}>
                                                            {translate("girls_clothing")}
                                                            <span className="sub-arrow"></span>
                                                        </h5>
                                                    </div>
                                                    <div className="menu-content">
                                                        <ul>
                                                            <li className="up-text">
                                                                <Link to={`${process.env.PUBLIC_URL}/girls-clothing/collection/frocks-and-skirts`}>
                                                                    {translate("froks_and_skits")}
                                                                    <span></span>
                                                                </Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/girls-clothing/collection/kurti-and-kamez`}>
                                                                    {translate("top_bottom")}
                                                                </Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/girls-clothing/collection/kurti-and-kamez`}>
                                                                    {translate("kurti_kamezz")}
                                                                </Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/girls-clothing/collection/lehenga`}>{translate("lehenga")}</Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/girls-clothing/collection/ghara`}>{translate("ghara")}</Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/girls-clothing/collection/gown`}>{translate("gown")}</Link>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col mega-box">
                                                <div className="link-section">
                                                    <div className="menu-title">
                                                        <h5 onClick={e => this.handleMegaSubmenu(e)}>
                                                            {translate("men_accessories")}
                                                            <span className="sub-arrow"></span>
                                                        </h5>
                                                    </div>
                                                    <div className="menu-content">
                                                        <ul>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/boys-clothing/collection/top-and-t-shirts`}>Tops And T-Shirts</Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/boys-clothing/collection/jeans-tousers-and-shorts`}>
                                                                    Jeans , Tousers And Shorts
                                                                </Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/boys-clothing/collection/suits-and-jackets`}>Suits And Jackets</Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/boys-clothing/collection/kurtas-and-sherwani`}>
                                                                    Kurtas And Sherwani
                                                                </Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/boys-clothing/collection/sets-and-outfits`}>Sets And Outfits</Link>
                                                            </li>
                                                            <li>
                                                                <Link to={`${process.env.PUBLIC_URL}/boys-clothing/collection/shirts`}>Shirts</Link>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/makup-&-beauty`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/makeup.png`} className="navIcon" alt="" />
                                    
                                    {translate("makupandbeauty")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/shoes`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/shoes.png`} alt="" />
                                    
                                    {translate("shoes")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/perfume-&-mist`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/perfume.png`} className="navIcon" alt="" />
                                    
                                    {translate("perfumeandmist")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/jewellery`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/jewellery.png`} className="navIcon" alt="" />
                                    
                                    {translate("jewellery")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/toys`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/toys.png`} className="navIcon" alt="" />
                                    
                                    {translate("toys")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/woman-&-men-accessories`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/man-woman-acc.png`} alt="" />
                                    
                                    {translate("womanandmenacc")}
                                </Link>
                            </li>
                            <li style={{margin: '0rem 0.6rem 0rem'}}>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/left-sidebar/collection/giftitem`}
                                    className="nav-link"
                                    onClick={e => this.handleSubmenu(e)}
                                >
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon/gift.png`} className="navIcon" alt="" />
                                    
                                    {translate("giftitem")}
                                </Link>
                            </li>

                            {/*<li >*/}
                            {/*<Link to={`${process.env.PUBLIC_URL}/blog/right-sidebar`} className="nav-link" onClick={(e) => this.handleSubmenu(e)}>*/}
                            {/*<FontAwesomeIcon icon={faNewspaper} className="navIcon" /><br></br>{translate('blog')}*/}
                            {/*</Link>*/}
                            {/*</li>*/}
                            {/*<li >*/}
                            {/*<Link to={`${process.env.PUBLIC_URL}/pages/contact`} className="nav-link" onClick={(e) => this.handleSubmenu(e)}>*/}
                            {/*<FontAwesomeIcon icon={faAddressBook} className="navIcon" /><br></br>{translate('contact')}*/}
                            {/*</Link>*/}
                            {/*</li>*/}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default withTranslate(NavBar);
