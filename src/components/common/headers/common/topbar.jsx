import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withTranslate } from "react-redux-multilingual";

class TopBar extends Component {
    constructor() {
        super();
        this.state = {
            token: "",
            author_name: "",
            picture: "",
            isLoading: false
        };
    }

    
    componentWillMount() {
        const tokenVar = localStorage.getItem("token");
        const author_name_var = localStorage.getItem("author_name");
        const picture_var = localStorage.getItem("author_picture");
        this.setState({
            token: tokenVar,
            author_name: author_name_var,
            picture: picture_var
        });
    }

    Logout(event) {
        localStorage.removeItem("token");
        window.location.href = "/login";
    }

    render() {
        const { translate } = this.props;
        const { token, author_name, picture} = this.state; //User Login Token      
        console.log(picture);  
        return (
            <div className="top-header">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="header-contact">
                                <ul>
                                    <li>{translate("topbar_title", { theme_name: " Multikart" })}</li>
                                    <li>
                                        <i className="fa fa-phone" aria-hidden="true"></i>
                                        {translate("call_us")}: +8801918818686
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-6 text-right">
                            <ul className="header-dropdown">
                                {/*<li className="mobile-wishlist compare-mobile"><Link to={`${process.env.PUBLIC_URL}/compare`}><i className="fa fa-random" aria-hidden="true"></i>{translate('compare')}</Link></li>*/}
                                {token ? (
                                    <li className="onhover-dropdown mobile-account">
                                        <i className="fa fa-user" aria-hidden="true"></i> {author_name}
                                        <ul className="onhover-show-div">
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/dashboard`} data-lng="en">
                                                    Dashboard
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/my-orders`} data-lng="en">
                                                    Your Orders
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={"#"} data-lng="en" onClick={this.Logout}>
                                                    Logout
                                                </Link>
                                            </li>
                                        </ul>
                                    </li>
                                ) : (
                                    <li>
                                        <i className="fa fa-user" aria-hidden="true"></i>{" "}
                                        <Link to={`${process.env.PUBLIC_URL}/login`} data-lng="en">
                                            Sign In
                                        </Link>
                                    </li>
                                )}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withTranslate(TopBar);
