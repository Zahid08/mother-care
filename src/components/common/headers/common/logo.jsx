import React from 'react';
import {Link} from 'react-router-dom'

function LogoImage(props) {

    if(props.enlarge) {
        return (
            <Link to={`${process.env.PUBLIC_URL}/`} >
                <img style={{width: '120%'}} className="logo-image" src={`${process.env.PUBLIC_URL}/assets/images/icon/${props.logo}`} alt=""/>
            </Link>
        )
    }
    else {
        return (
            <Link to={`${process.env.PUBLIC_URL}/`} >
                <img className="logo-image" src={`${process.env.PUBLIC_URL}/assets/images/icon/${props.logo}`} alt=""/>
            </Link>
        )
    }
}

export default LogoImage;