import React, {Component} from 'react';
import Slider from 'react-slick';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'

import {getBestSellerNewProductDetails} from "../../services";


class NewProductDetails extends Component {
    render (){
        const {items, symbol} = this.props;

        var arrays = [];
        while (items.length > 0) {
            arrays.push(items.splice(0, 3));
        }

        return (
            <div className="theme-card">
                <h5 className="title-border">new product</h5>
                <Slider className="offer-slider slide-1">
                    {arrays.map((products, index) =>
                        <div key={index}>
                            {products.map((product, i) =>
                                <div className="media" key={i}>
                                    <Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product.id}`}><img className="img-fluid" src={`${
                                        product.variants?
                                            product.variants[0].images
                                            :product.pictures[0]
                                        }`}  alt="" /></Link>
                                    <div className="media-body align-self-center">
                                        <div className="rating">
                                            {(() => {
                                                const RatingStars = [];
                                                for(var i = 0; i < product.rating; i++) {
                                                    RatingStars.push(<i className="fa fa-star" key={i}></i>)
                                                }
                                                return RatingStars;
                                            })()}
                                        </div>
                                        <Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product.id}`}><h6 style={{"width":"100%"}}>{product.name}</h6></Link>
                                        {/*<h4>{symbol}{(product.price*product.discount/100)}*/}
                                            {/*<del><span className="money">{symbol}{product.price}</span></del></h4>*/}
                                        <h4>{symbol}{product.salePrice}</h4>
                                    </div>
                                </div>
                            )}
                        </div>
                    )}
                </Slider>
            </div>
        )
    }
}

function mapStateToProps(state,ownProps) {
    return {
        items: getBestSellerNewProductDetails(state.data.products),
        symbol: state.data.symbol
    }
}

export default connect(mapStateToProps, null)(NewProductDetails);
