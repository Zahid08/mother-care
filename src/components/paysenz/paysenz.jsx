import React, {Component} from 'react';
import API from "../../config";

const requestData = { 'grant_type': 'password', 'client_id': 16, 'client_secret': '2XeYHZSAorBmqowLmCOUXFWLTb21kGRlekEfCe46','username':'info@unlocklive.com','password':'123456','scope':'*' };
class Paysenz extends Component {
    constructor(){
        super();
        this.state = {
            paysenztoken : '',
            orderId:'',
            successUrl: `${API.site_url}/order-success`, //`${process.env.PUBLIC_URL}/order-success`
            failUrl: `${API.site_url}/order-success`, //`${process.env.PUBLIC_URL}/order-success`,
            cancelUrl: `${API.site_url}/order-success`, //`${process.env.PUBLIC_URL}/order-success`,
            ipnUrl:`${API.success_order}`,//`${process.env.PUBLIC_URL}/order-success`,
        }
    }
    componentWillMount(){
        this.setState({orderId:1 + (Math.random() * (100-1))})
        this.getRetriveToken(requestData);
    }
    //Retrive Access Token from paysenz
    getRetriveToken=(requestData)=>{
        fetch(`${API.gateway.authinticate}`, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            },
            body: JSON.stringify({
                grant_type: "password",
                client_id: `${API.gateway.client_id}`,
                client_secret:`${API.gateway.client_secret}`,
                username: `${API.gateway.username}`,
                password:`${API.gateway.password}`,
                scope: "*"
            })
        }).then(resp => resp.json())
            .then(data => {
                 this.setState({paysenztoken:data});
                 this.sendInformationToPaysenz(data.access_token,this.props.billing);
            })
    }

    sendInformationToPaysenz(token,data) {
        console.log("Billing Information for paysenz");
        console.log(data);
        if (data) {
            let Json={
                'buyer_name': data.first_name ? data.first_name : '',
                'buyer_email': data.email ? data.email : '',
                'buyer_address': data.address ? data.address : '',
                'cus_city': 'City',
                'cus_state': 'State',
                'cus_postcode': data.pincode ? data.pincode : '',
                'cus_country': 'Bangladesh',
                'buyer_contact_number': data.phone ? data.phone : '',
                'client_id': requestData.client_id ? requestData.client_id : '',
                'order_id_of_merchant': this.state.orderId ? 'Paysez' +this.state.orderId : '',
                'amount':'1',//this.props.orderTotal ? String(this.props.orderTotal ): '',
                'currency_of_transaction': 'BDT',
                'callback_success_url': this.state.successUrl ? this.state.successUrl : '',
                'callback_fail_url': this.state.failUrl ? this.state.failUrl : '',
                'callback_cancel_url': this.state.cancelUrl ? this.state.cancelUrl : '',
                'callback_ipn_url': this.state.ipnUrl ?this.state.ipnUrl : '',
                'order_details': this.state.orderId ? 'Payment for ApplicationID:' + this.state.orderId : '',
                'expected_response_type': 'JSON',
                'custom_1':data.order_id ?String(data.order_id) : '',
                'custom_2':data.order_number ?String(data.order_number) : '',
                'custom_3':data.user_id ?String(data.user_id) : '',
                'custom_4':'',
            };
            fetch(`${API.gateway.payment}`, {
                method: "post",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(Json)
            }).then(resp => resp.json())
                .then(paysenz_response => {
                    if (paysenz_response.expected_response) {
                       const url = paysenz_response.expected_response;
                       window.location = url;
                       console.log(Json);
                        localStorage.setItem("payment_method", 'paysenz');
                    }
                })
        }
    }

    render (){
        return (
            <div className="col-lg-12">
                <h3 className="loder_style" style={{textAlign: "Left",color:"#000",display:"inline-block",fontWeight:"bold"}}>Your Order Is Processing<img style={{width:"10%",display:"inline-block",marginLeft:"20px"}} src={`${process.env.PUBLIC_URL}/assets/images/icon/loding.gif`}/></h3>
            </div>
        )
    }
}

export default Paysenz