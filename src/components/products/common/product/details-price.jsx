import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import Slider from 'react-slick';
import Modal from 'react-responsive-modal';
import {Helmet} from 'react-helmet'
import { addToCart } from "../../../../actions";
import { connect } from "react-redux";
import {
    FacebookShareButton,
    FacebookIcon,
    TwitterShareButton,
    TwitterIcon,
    EmailShareButton,
    EmailIcon,
    LinkedinShareButton,
    LinkedinIcon
} from 'react-share';
import {toast} from "react-toastify";

class DetailsWithPrice extends Component {

    constructor (props) {
        super (props)
        this.state = {
            open:false,
            stock: 'InStock',
            quantity: 1,
            size: "",
            color: "",
            stockStatus: "InStock",
            filteredCurrentStock: null,
            nav3: null,
            reviewCount: null,
            uniqueColors: [],
            uniqueSize: [],
            currentSalePrice:"",
            currentDiscount:"",
        }
    }

    onOpenModal = () => {
        this.setState({ open: true });
    };

    onCloseModal = () => {
        this.setState({ open: false });
    };

    componentDidMount() {
        const { item } = this.props;
        //if veriation created
        if (item.variation_type === 2 && item.variants.length>0) {
            this.setState({ color: item.variants[0].color });
            this.setState({ size: item.variants[0].size });
            this.setState({ filteredCurrentStock: item.variants[0].stock });
            this.setState({ currentSalePrice: item.variants[0].salePrice });
            this.props.onSelectImage(`${item.variants[0].images}`);
            var uniqueColors = [];
            item.variants.map((variationItem, index) => {
                uniqueColors.push(variationItem.color);
            });
            var uniqueSize = [];
            item.variants.map((variationItem, index) => {
                uniqueSize.push(variationItem.size);
            });
            this.setState({ uniqueColors }, this.setState({ uniqueSize }, this.calculateCurrentStock));
        }else {
            let stock=0;
            if (item.stock=='out_of_stock'){
                stock=0;
            }else{
                stock=item.stock;
            }
            this.setState({ filteredCurrentStock: stock });
        }

        this.setState({
            nav3: this.slider3
        });
    }

    minusQty = () => {
        const { item } = this.props;

        if(this.state.quantity > 1) {
            this.setState({stock: 'InStock'})
            this.setState({quantity: this.state.quantity - 1})
        }
    }

    plusQty = () => {
        if(this.props.item.stock > this.state.quantity) {
            this.setState({quantity: this.state.quantity+1})
        }else{
            this.setState({stock: 'Out of Stock !'})
        }
    }
    changeQty = (e) => {
        this.setState({ quantity: parseInt(e.target.value) })
    }

    calculateCurrentStock = () => {
        const { color, size } = this.state;
        const { item } = this.props;
        //back to child component Image
        if (item.variation_type == 2) {
            //Get Only Colour WIse Image Path
            let variationItems = item.variants.find(item => {
                if (item.color == color) {
                    return item;
                }
            });
            this.props.onSelectImage(`${variationItems.images}`);


            //Get Colour and size wise Price and discount
            let priceGet = item.variants.find(item => {
                if (item.color == color && item.size == size) {
                    return item;
                }
            });

           if (priceGet!='undefined'){
               if (priceGet){
                   this.setState({ filteredCurrentStock: priceGet.stock});
               }else {
                   this.setState({ filteredCurrentStock: ''});
               }

           }else {
               toast.error("Out of Stock !");
               this.setState({ stockStatus: "Out of Stock !" });
           }

            if(typeof priceGet=='undefined'){
                this.setState({ currentSalePrice: item.salePrice });
                //this.setState({ filteredCurrentStock: item.stock });
                //this.setState({ currentDiscount: item.discount });
            }else{
                this.setState({ currentSalePrice: priceGet.salePrice });
                //this.setState({ filteredCurrentStock: priceGet.stock });
                //this.setState({ currentDiscount: priceGet.discount });
            }
        }
    };

    handleAddToCart = event => {

        let { item } = this.props;
        let { color, size, quantity, filteredCurrentStock } = this.state;

        let getSttaus=false;
        let variation;
        if (filteredCurrentStock - quantity >= 0 || filteredCurrentStock=='in_stock' || filteredCurrentStock=='limited' ||  filteredCurrentStock=='infinite'){
            if (item.variation_type != 2) {
                this.props.addToCart(item, quantity,'','','','');
            } else {
                if (item.variants.length>0){
                    variation = item.variants.find(item => {
                        if (item.color == color && item.size == size) {
                            return item;
                        }
                    });
                    if(variation){
                        this.props.addToCart(variation, quantity, color, size, variation.id,variation.image);
                    }
                }
                else {
                    toast.error("ERROR");
                }
            }
        } else {
            this.setState({ stockStatus: "Out of Stock !" });
            toast.error("Out of Stock !");
        }
    };

    render (){
        const {symbol, item, addToCartClicked, BuynowClicked, addToWishlistClicked} = this.props
        const {uniqueColors,uniqueSize ,currentSalePrice,filteredCurrentStock} = this.state;

        var colorsnav = {
            slidesToShow: 6,
            swipeToSlide:true,
            arrows: false,
            dots: false,
            focusOnSelect: true
        };

        return (
            <div className="col-lg-6 rtl-text">
                <Helmet>
                     <title>MultiKart | {item.category} | {item.name}</title>
                    <meta property="og:url" content={`http://ecom2.unlockretail.com/left-sidebar/product/${item.id}`}/>
                    <meta property="og:description"  content={item.description?item.description:'blank_description'} />
                    <meta property="og:image"        content={item.pictures[0]?item.pictures[0]:''} />
                    <meta property="fb:app_id" content="1042242282782239" />
                </Helmet>
                <div className="product-right">
                    <h2> {item.name} </h2>
                    <h2>SKU CODE : {item.squ_code} </h2>
                    {/*<h4>*/}
                        {/*<del>{symbol}{item.price}</del>*/}
                        {/*<span>{item.discount}% off</span></h4>*/}
                    {/*<h3>{symbol}{item.price-(item.price*item.discount/100)} </h3>*/}
                    <h3>{symbol}  {currentSalePrice?currentSalePrice:item.salePrice} </h3>
                    {item.variants?
                    <ul >
                        <Slider {...colorsnav} asNavFor={this.props.navOne} ref={slider => (this.slider1 = slider)} className="color-variant">
                            {item.variants.map((vari, i) => {
                                return <li className={vari.color} key={i} title={vari.color}></li>
                            })}
                        </Slider>
                    </ul>:''}
                    <div className="product-description border-product">
                        {item.variation_type === 2 ? (
                            <div>
                                <h6 className="product-title size-text">Size</h6>

                                <div className="size-box">
                                    <ul>
                                        {uniqueSize.map((s, i) => {
                                            if (s.toString() == this.state.size) {
                                                return (
                                                    <li className="front-check-single-active"
                                                        value={s.toString()}
                                                        onClick={e => {
                                                            this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                                        }}
                                                        key={i}
                                                        id={i}
                                                    >
                                                        {s}
                                                    </li>
                                                );
                                            } else {
                                                return (
                                                    <li className="front-check-single"
                                                        value={s.toString()}
                                                        onClick={e => {
                                                            this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                                        }}
                                                        key={i}
                                                        id={i}
                                                    >
                                                        {s}
                                                    </li>
                                                );
                                            }
                                        })}
                                    </ul>
                                </div>
                            </div>
                        ) : (
                            ""
                        )}

                        {item.variation_type === 2 ? (
                            <div>
                                <h6 className="product-title size-text">Color</h6>

                                <div className="size-box">
                                    {uniqueColors.map(color => {
                                        if (color.length > 1) {
                                            if (color == this.state.color) {
                                                return (
                                                    <li
                                                        value={color.toString()}
                                                        onClick={e => {
                                                            console.log(e.currentTarget.getAttribute("value"));
                                                            this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                                        }}
                                                        style={{
                                                            background: `${color}`,
                                                            width: "25px",
                                                            height: "25px",
                                                            border: "3px solid #fff",
                                                            boxShadow: "0 0 0 3px hsl(0, 0%, 80%)",
                                                            borderRadius: "50%",
                                                            marginRight:"10px"
                                                        }}
                                                    ></li>
                                                );
                                            } else {
                                                return (
                                                    <li
                                                        value={color.toString()}
                                                        onClick={e => {
                                                            console.log(e.currentTarget.getAttribute("value"));
                                                            this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                                        }}
                                                        style={{ background: `${color}`, width: "25px", height: "25px", border: "1px solid #000", borderRadius: "50%",marginRight:"10px" }}
                                                    ></li>
                                                );
                                            }
                                        }
                                    })}
                                </div>
                            </div>
                        ) : (
                            ""
                        )}

                        {/*{item.size?*/}
                            {/*<div>*/}
                                {/*<h6 className="product-title size-text">select size*/}
                                    {/*<span><a href="#" data-toggle="modal"*/}
                                             {/*data-target="#sizemodal" onClick={this.onOpenModal} >size chart</a></span></h6>*/}
                                {/*<div className="modal fade" id="sizemodal" tabIndex="-1"*/}
                                     {/*role="dialog" aria-labelledby="exampleModalLabel"*/}
                                     {/*aria-hidden="true">*/}
                                    {/*<div className="modal-dialog modal-dialog-centered"*/}
                                         {/*role="document">*/}
                                        {/*<div className="modal-content">*/}
                                            {/*<div className="modal-header">*/}
                                                {/*<h5 className="modal-title"*/}
                                                    {/*id="exampleModalLabel">Sheer Straight*/}
                                                    {/*Kurta</h5>*/}
                                                {/*<button type="button" className="close"*/}
                                                        {/*data-dismiss="modal" aria-label="Close">*/}
                                                    {/*<span aria-hidden="true">&times;</span>*/}
                                                {/*</button>*/}
                                            {/*</div>*/}
                                            {/*<div className="modal-body">*/}
                                                {/*<img src={`${process.env.PUBLIC_URL}/assets/images/size-chart.jpg`} alt="" className="img-fluid"/>*/}
                                            {/*</div>*/}
                                        {/*</div>*/}
                                    {/*</div>*/}
                                {/*</div>*/}
                                {/*<div className="size-box">*/}
                            {/*<ul>*/}
                                {/*{item.size.map((size, i) => {*/}
                                    {/*return <li key={i}><a href="#">{size}</a></li>*/}
                                {/*})}*/}
                            {/*</ul>*/}
                        {/*</div>*/}
                            {/*</div>:''}*/}

                        {item.variation_type === 2 ? (
                            <span className="instock-cls">{filteredCurrentStock?filteredCurrentStock:'Out of Stock'}</span>
                        ) : (
                            <span className="instock-cls">{filteredCurrentStock?filteredCurrentStock:'Out of Stock'}</span>
                        )}

                        <h6 className="product-title">quantity</h6>
                        <div className="qty-box">
                            <div className="input-group">
                                  <span className="input-group-prepend">
                                    <button type="button" className="btn quantity-left-minus" onClick={this.minusQty} data-type="minus" data-field="">
                                     <i className="fa fa-minus"></i>
                                    </button>
                                  </span>
                                <input type="text" name="quantity" value={this.state.quantity} onChange={this.changeQty} className="form-control input-number" />
                                <span className="input-group-prepend">
                                <button type="button" className="btn quantity-right-plus" onClick={this.plusQty} data-type="plus" data-field="">
                                <i className="fa fa-plus"></i>
                                </button>
                               </span>
                            </div>
                        </div>
                    </div>
                    <div className="product-buttons" >
                        {/*{item.variation_type === 2 ? (*/}
                            {/*<a className="btn btn-solid" onClick={() => this.handleAddToCart}>add to cart</a>*/}
                        {/*) : (*/}
                            {/*<a className="btn btn-solid" onClick={() => this.handleAddToCart}>add to cart</a>*/}
                        {/*)}*/}
                        <a className="btn btn-solid btn-cart " onClick={this.handleAddToCart}>
                           add to cart
                        </a>
                        <Link to={`${process.env.PUBLIC_URL}/checkout`} className="btn btn-solid" onClick={() => BuynowClicked(item, this.state.quantity)} >buy now</Link>
                    </div>
                    {/*<div className="border-product">*/}
                        {/*<h6 className="product-title">Product Short details</h6>*/}
                        {/*<p>{item.shortDetails}</p>*/}
                    {/*</div>*/}
                    <div className="border-product">
                        <h6 className="product-title">share it</h6>
                        <div className="product-icon">
                            <ul className="product-social">
                                <li>
                                    <FacebookShareButton url={`http://ecom2.unlockretail.com/left-sidebar/product/${item.id}`} quote={item.name?item.name:''} className="share">
                                    <FacebookIcon size={32} round={true}/>
                                    </FacebookShareButton>
                                </li>
                                <li>
                                    <EmailShareButton url={`http://ecom2.unlockretail.com/left-sidebar/product/${item.id}`} subject={item.name?item.name:''} body={item.description?item.description:''}>
                                        <EmailIcon size={32} round={true}/>
                                    </EmailShareButton>
                                </li>
                                <li>
                                    <TwitterShareButton url={`http://ecom2.unlockretail.com/left-sidebar/product/${item.id}`} title={item.name?item.name:''}>
                                       <TwitterIcon size={32} round={true}/>
                                    </TwitterShareButton>
                                </li>
                                <li>
                                    <LinkedinShareButton url={`http://ecom2.unlockretail.com/left-sidebar/product/${item.id}`} title={item.name?item.name:''}>
                                        <LinkedinIcon size={32} round={true}/>
                                    </LinkedinShareButton>
                                </li>
                            </ul>
                                <button className="wishlist-btn" onClick={() => addToWishlistClicked(item)}><i
                                    className="fa fa-heart"></i><span
                                    className="title-font">Add To WishList</span>
                                </button>
                        </div>
                    </div>
                </div>
                <Modal open={this.state.open} onClose={this.onCloseModal} center>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Sheer Straight Kurta</h5>
                            </div>
                            <div className="modal-body">
                                <img src={`${process.env.PUBLIC_URL}/assets/images/size-chart.jpg`} alt="" className="img-fluid" />
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    language: state.Intl.locale,
    symbol: state.data.symbol,
    cartItems: state.cartList.cart
});

export default connect(mapStateToProps, { addToCart })(DetailsWithPrice);