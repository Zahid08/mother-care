import React, {Component} from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.scss';
import {Link} from 'react-router-dom'
import {toast} from "react-toastify";
import Breadcrumb from "../../common/breadcrumb";
import API from "../../../config";
class DetailsTopTabs extends Component {
    constructor(props,context){
        super (props,context)
        this.state = {
            token : '',
            ReviewSubject : '',
            ReviewRating : '',
            ReviewText : '',
            product_id : '',
            author_id : '',
            error_subject:'',
            error_rating:'',
            error_text:'',
            ratingItem:'',
        }
        this.ReviewSubject = this.ReviewSubject.bind(this);
        this.ReviewRating = this.ReviewRating.bind(this);
        this.ReviewText = this.ReviewText.bind(this);
        this.RegistrationReview = this.RegistrationReview.bind(this);
    }
    componentWillMount(){
        const tokenVar = localStorage.getItem('token');
        const author_id = localStorage.getItem('author_id');
        this.setState({
            token:tokenVar,
            author_id:author_id,
            product_id:this.props.item.id
        });
        this.getRatingList(this.props.item.id);
    }

    ReviewSubject(event) {
        this.setState({ ReviewSubject: event.target.value })
    }
    ReviewRating(event) {
        this.setState({ ReviewRating: event.target.value })
    }
    ReviewText(event) {
        this.setState({ ReviewText: event.target.value })
    }

    getRatingList(product_id) {
        if (product_id){
            fetch(`${API.api_root_url}/api/v1/e-commerce/shop/rating/${product_id}`, {
          method: "GET",
          headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
          }
      }).then(resp => resp.json())
          .then(response => {
              this.setState({
                  ratingItem: response
              })
          })
        }
    }

    RegistrationReview(event) {
     fetch(`${API.rating}`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            },
            body: JSON.stringify({
                review_subject: this.state.ReviewSubject,
                review_rating: this.state.ReviewRating,
                review_text: this.state.ReviewText,
                criteria: '',
                product_id:this.state.product_id,
                author_id:1,
            })
        }).then((Response) => Response.json())
            .then((result) => {
                if (result.status=='401'){
                    this.setState({'error_subject':result.errors.review_subject?result.errors.review_subject:''})
                    this.setState({'error_rating':result.errors.review_rating?result.errors.review_rating:''})
                    this.setState({'error_text':result.errors.review_text?result.errors.review_text:''})
                }else {
                    event.preventDefault();
                    this.setState({ReviewSubject: '',ReviewText:'',ReviewRating:''});
                    toast.success("Your review has been added successfully");
                }
            })
    }

    render (){
        const {item} = this.props;
        const {token,author_id,ratingItem} =this.state; //User Login Token

        let RatingStars = []
        for(var i = 0; i < item.rating; i++) {
            RatingStars.push(<i className="fa fa-star" key={i}></i>)
        }

        return (
            <section className="tab-product m-0">
                <div className="row">
                    <div className="col-sm-12 col-lg-12">
                        <Tabs className="tab-content nav-material">
                            <TabList className="nav nav-tabs nav-material">
                                <Tab className="nav-item">
                                    <span className="nav-link active" >
                                        <i className="icofont icofont-contacts"></i>Description</span>
                                    <div className="material-border"></div>
                                </Tab>
                                <Tab className="nav-item">
                                    <span className="nav-link active" >
                                        <i className="icofont icofont-contacts"></i>Write Review</span>
                                    <div className="material-border"></div>
                                </Tab>
                            </TabList>
                            <TabPanel>
                                <div dangerouslySetInnerHTML={ { __html: item.description } }></div>
                            </TabPanel>
                            <TabPanel>
                                <form className="theme-form mt-4">
                                    <div className="form-row">
                                        <div className="col-md-12 ">
                                            <div className="media m-0">
                                                <label>Rating</label>
                                                <div className="media-body ml-3">
                                                    <div className="rating three-star">
                                                        {RatingStars}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {ratingItem.length > 0 ?
                                            <div className="col-md-12">
                                                {ratingItem.map((item, index) => {
                                                    return (
                                                        <div className="comment-body" key={index}>
                                                            <div className="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-md-1">
                                                                            <img src={item.author_image}
                                                                                 className="img img-rounded img-fluid"/>
                                                                        </div>
                                                                        <div className="col-md-11">
                                                                            <p>
                                                                           <strong>{item.author_name}</strong>
                                                                                {(() => {
                                                                                    const RatingStars = [];
                                                                                    for(var i = 0; i < item.rating; i++) {
                                                                                        RatingStars.push( <span className="float-right"  key={i}><i
                                                                                            className="text-warning fa fa-star"></i></span>)
                                                                                    }
                                                                                    return RatingStars;
                                                                                })()}
                                                                            </p>
                                                                            <div className="clearfix"></div>
                                                                            <p className="text-secondary float-left created-at"><i className="fa fa-calendar"></i> {item.created_at}</p><br/>
                                                                            <p className="float-left content-body">{item.body?item.body:''}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> )
                                                })}
                                            </div>
                                            :''
                                        }
                                    </div>
                                </form>
                                        {token?
                                            <form className="theme-form mt-4">
                                                <div className="form-row">
                                        <div className="col-md-6 mg-bottom-10">
                                            <label htmlFor="name">Subject<span className='error_rating'>*</span></label>
                                            <input type="text" onChange={this.ReviewSubject} className="form-control" id="ReviewSubject" placeholder="Enter Subject" value={this.state.ReviewSubject} required />
                                            <span className='error_rating'>{this.state.error_subject}</span>
                                            </div>
                                        <div className="col-md-6">
                                            <label htmlFor="email">Rating <span className='error_rating'>*</span></label>
                                            <select className="form-control "  value={this.state.ReviewRating} id="review_rating" onChange={this.ReviewRating}>
                                                <option selected="selected" value="">Select Rating ...</option>
                                                <option value="5">5 Stars</option>
                                                <option value="4">4 Stars</option>
                                                <option value="3">3 Stars</option>
                                                <option value="2">2 Stars</option>
                                                <option value="1">1 Star</option>
                                            </select>
                                            <span className='error_rating'>{this.state.error_rating}</span>
                                        </div>
                                        <div className="col-md-12">
                                            <label htmlFor="review">Review Title<span className='error_rating'>*</span></label>
                                            <textarea onChange={this.ReviewText} className="form-control " placeholder="Wrire Your Review About Product" id="ReviewText" rows="6" value={this.state.ReviewText}></textarea>
                                            <span className='error_rating'>{this.state.error_text}</span>
                                        </div>
                                        <div className="col-md-12">
                                            <a className="btn btn-solid review-butoon"  onClick={this.RegistrationReview}>Submit Your Review</a>
                                        </div>
                                    </div>
                                </form>
                                    :
                                    <div
                                        className="alert alert-info alert-dismissible fade show text-center margin-bottom-1x">
                                        <span className="alert-close" data-dismiss="alert"></span><i
                                        className="icon-layers"></i>You need to login to be able to leave a review.
                                    </div>
                                }
                            </TabPanel>
                        </Tabs>
                    </div>
                </div>
            </section>
        )
    }
}

export default DetailsTopTabs;