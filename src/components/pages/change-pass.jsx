import React, {Component} from 'react';
import Breadcrumb from "../common/breadcrumb";
import {Link} from 'react-router-dom'
import DashboardLeft from "./dashboard-left";

class ChangePass extends Component {

    constructor (props) {
        super (props)
    }

    render (){


        return (
            <div>
            <Breadcrumb title={'Change Password'}/>
            {/*Dashboard section*/}
            <section className="section-b-space">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3">
                            <div className="account-sidebar">
                                <a className="popup-btn">
                                    my account
                                </a>
                            </div>
                            <DashboardLeft/>
                        </div>
                        <div className="col-lg-9">
                            <div className="dashboard-right">
                                {/*Forget Password section*/}
                                <section className="pwd-page" style={{paddingTop: 0}}>
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-lg-6 offset-lg-3">
                                                <h2>change Your Password</h2>
                                                <form className="theme-form">
                                                    <div className="form-row">
                                                        <div className="col-md-12">
                                                            <input type="text" className="form-control" id="old"
                                                                placeholder="Enter Your Old Password" required="" />
                                                            <input type="text" className="form-control" id="new"
                                                                placeholder="Enter Your New Password" required="" />
                                                            <input type="text" className="form-control" id="confirm"
                                                                placeholder="Enter Confirm Password" required="" />
                                                        </div>
                                                        <a href="#" className="btn btn-solid">Submit</a>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </section>                                 
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            </div>            
        )
    }
}

export default ChangePass