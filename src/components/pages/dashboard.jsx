import React, {Component} from 'react';
import Breadcrumb from "../common/breadcrumb";
import {Link} from "react-router-dom";
import MyOrders from './my-orders';
import wishList from '../wishlist/index';
import ChangePass from './change-pass';
import DashboardLeft from "./dashboard-left";
import API from "../../config";


class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            token : '',
            profileItems:''
        }
    }
    componentWillMount(){
        const tokenVar = localStorage.getItem('token');
        if (tokenVar) {
            this.getProfile(tokenVar);
        }
    }

    getProfile(tokenVar) {
        console.log(tokenVar);
        fetch(`${API.profile}`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                 'Accept': 'application/json',
                'Authorization': `${tokenVar}`,
                'X-Requested-With': 'XMLHttpRequest',
            }
        }).then(resp => resp.json())
            .then(response => {
                this.setState({
                    profileItems: response.data
                })
            })
    }

    render (){
        var tokenCheck = localStorage.getItem('token');
        if (tokenCheck==null || typeof(tokenCheck)=='undefined'){
            this.props.history.push("/login");
        }
        const {profileItems}=this.state;

        console.log(profileItems);
        return (
            <div>
                <Breadcrumb title={'Dashboard'}/>
                {/*Dashboard section*/}
                <section className="section-b-space">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3">
                                <div className="account-sidebar">
                                    <a className="popup-btn">
                                        my account
                                    </a>
                                </div>
                                 <DashboardLeft/>
                            </div>
                            <div className="col-lg-9">
                                <div className="dashboard-right">
                                    <div className="dashboard">
                                        <div className="page-title">
                                            <h2>Dashboard</h2>
                                        </div>
                                        <div className="welcome-msg">
                                            <p>Hello, {profileItems.full_name} !</p>
                                        </div>
                                        <div className="box-account box-info">
                                            <div className="box-head">
                                                <h2>Account Information</h2>
                                            </div>
                                            <div>
                                                <div className="box">
                                                    <div className="box-title">
                                                        <h3>Contact Information</h3>
                                                        <a href="#">Manage Contact</a>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-sm-6">
                                                            <h6>{profileItems.full_name}</h6>
                                                            <h6>{profileItems.email}</h6>
                                                            <h6>{profileItems.phone_number}</h6>
                                                            <h6><a href={`${process.env.PUBLIC_URL}/change-pass`}>Change Password</a></h6>
                                                        </div>
                                                        <div className="col-sm-6">
                                                            <div className="col-md-3">
                                                                <img src={profileItems.picture_thumb}
                                                                     className="img img-rounded img-fluid"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <div className="box">
                                                    <div className="box-title">
                                                        <h3>Address Book</h3>
                                                        {/*<a href="#">Manage Addresses</a>*/}
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-sm-6">
                                                            <h6> Billing Address </h6>
                                                            <address>
                                                                You have not set a default billing address.<br/>
                                                            </address>
                                                        </div>
                                                        <div className="col-sm-6">
                                                            <h6>Default Shipping Address</h6>
                                                            <address>
                                                                You have not set a default shipping address.<br />
                                                            </address>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                                                       
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

export default Dashboard