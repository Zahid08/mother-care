import React, {Component} from 'react';
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'


import Breadcrumb from '../common/breadcrumb';
import {addToCartAndRemoveWishlist, removeFromWishlist} from '../../actions'
import DashboardLeft from "./dashboard-left";
import API from "../../config";
let BASE_URL = `${API.api_root_url}`;
const API_URL = `${BASE_URL}/api/v1/e-commerce/orders/my-orders`;

class MyOrders extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderitems: [],
            token : '',
        }
    }
    componentDidMount() {
        const tokenVar = localStorage.getItem('token');
      if (tokenVar) {
          this.getOrderList(tokenVar);
      }
    }

    getOrderList(tokenVar) {
        fetch(API_URL, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `${tokenVar}`
            }
        }).then(resp => resp.json())
            .then(response => {
                this.setState({
                    orderitems: response.data
                })
            })
    }

    changeQty = (e) => {
        this.setState({ quantity: parseInt(e.target.value) })
    }

    render (){
        const {orderitems} = this.state;
        //Session Destroy
        var tokenCheck = localStorage.getItem('token');
        if (tokenCheck==null || typeof(tokenCheck)=='undefined'){
            this.props.history.push("/login");
        }
        return (
            <div>
            <Breadcrumb title={'My Orders'}/>
            {/*Dashboard section*/}
            <section className="section-b-space">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3">
                            <div className="account-sidebar">
                                <a className="popup-btn">
                                    my account
                                </a>
                            </div>
                            <DashboardLeft/>
                        </div>
                        <div className="col-lg-9">
                            <div className="dashboard-right">
                                <div className="dashboard">
                            {orderitems.length>0 ?
                            <table className="table cart-table table-responsive-xs">
                                <thead>
                                <tr className="table-head">
                                    <th scope="col">ID</th>
                                    <th scope="col">Product Image</th>
                                    <th scope="col">Order Number</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Payment Status</th>
                                    <th scope="col">Created At</th>
                                </tr>
                                </thead>
                                {orderitems.map((item, index) => {
                                    return (
                                        <tbody key={index++}>
                                        <tr>
                                            <td><p style={{textAlign: "Left",color:"#000",display:"inline-block",fontWeight:"bold"}}>{index}</p></td>
                                            <td> <img height="50" width="50" src={item.billing.billing_address.image?
                                                item.billing.billing_address.image
                                                :''} alt="" /></td>
                                            <td><p style={{textAlign: "Left",color:"#000",display:"inline-block",fontWeight:"bold"}}>{item.order_number}</p></td>
                                            <td><p style={{textAlign: "Left",color:"#000",display:"inline-block",fontWeight:"bold"}}>{item.amount}</p></td>
                                            <td><p style={{textAlign: "Left",color:"#000",display:"inline-block",fontWeight:"bold"}}>{item.billing.payment_status}</p></td>
                                            <td><p style={{textAlign: "Left",color:"#000",display:"inline-block",fontWeight:"bold"}}>{item.created_at}</p></td>
                                        </tr>
                                        </tbody> )
                                })}
                            </table>
                                :
                                <section className="cart-section section-b-space">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div >
                                                    <div className="col-sm-12 empty-cart-cls text-center">
                                                        <img src={`${process.env.PUBLIC_URL}/assets/images/empty-wishlist.png`} className="img-fluid mb-4" alt="" />
                                                        <h3>
                                                            <strong>Order List is Empty</strong>
                                                        </h3>
                                                        <h4>Explore more shortlist some items.</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            }                                  
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    Items: state.wishlist.list,
    symbol: state.data.symbol,
})

export default connect(
    mapStateToProps,
    {addToCartAndRemoveWishlist, removeFromWishlist}
)(MyOrders)