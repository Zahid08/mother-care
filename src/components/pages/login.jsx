import React, { Component } from "react";

import Breadcrumb from "../common/breadcrumb";
import { Link } from "react-router-dom";
import { userPostFetch } from "../../services";
import { connect } from "react-redux";
import Facebook from "../facebook";
import GoogleLogin from "react-google-login";
import API from "../../config";

class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      Email: "",
      Password: "",
      error_email: "",
      error_password: "",
      register_first_name: "",
      register_last_name: "",
      register_email: "",
      register_password: "",
      register_c_password: "",
      register_phone: "",
      register_first_name_error: "",
      register_last_name_error: "",
      register_email_error: "",
      register_password_error: "",
      register_c_password_error: "",
      register_phone_error: "",
      registration_success_message: ""
    };

    this.Password = this.Password.bind(this);
    this.Email = this.Email.bind(this);
    this.login = this.login.bind(this);

    this.register_first_name = this.register_first_name.bind(this);
    this.register_last_name = this.register_last_name.bind(this);
    this.register_email = this.register_email.bind(this);
    this.register_password = this.register_password.bind(this);
    this.register_c_password = this.register_c_password.bind(this);
    this.register_phone = this.register_phone.bind(this);
    this.Registration = this.Registration.bind(this);
  }
  Email(event) {
    this.setState({ Email: event.target.value });
  }
  Password(event) {
    this.setState({ Password: event.target.value });
  }
  register_first_name(event) {
    this.setState({ register_first_name: event.target.value });
  }
  register_last_name(event) {
    this.setState({ register_last_name: event.target.value });
  }
  register_email(event) {
    this.setState({ register_email: event.target.value });
  }
  register_password(event) {
    this.setState({ register_password: event.target.value });
  }
  register_c_password(event) {
    this.setState({ register_c_password: event.target.value });
  }
  register_phone(event) {
    this.setState({ register_phone: event.target.value });
  }
  responseGoogle = response => {
    console.log("GOOGLE", response);
    console.log(typeof response.profileObj);
    if (typeof response.profileObj !== "undefined") {
      console.log("in");
      let googleName = response.profileObj.name;
      let googleUsername = response.profileObj.name + "_g";
      let googleEmail = response.profileObj.email;
      let googlePassword = response.profileObj.googleId;
      fetch(`${API.register}`, {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "X-Requested-With": "XMLHttpRequest"
        },
        body: JSON.stringify({
          name: googleName,
          last_name: googleUsername,
          email: googleEmail,
          password: googlePassword,
          password_confirmation: googlePassword,
          terms: 1
        })
      }).then(Response => Response.json())
        .then(result => {
          console.log(result.data);
          if (result.errors) {
            console.log("Error");
          } else {
            console.log("Success");
            console.log(result);
            localStorage.setItem("token", result.data.authorization);
            window.location.href = "/checkout";
          }
          //   if (result.data.status === 422) {
          //     const user = JSON.parse(result.data.message);
          //     console.log(result);
          //     if (user.username[0] == "This username has already been taken.") {
          //       fetch("http://mothercare.unlockretail.com/api/v1/auth/register", {
          //         method: "post",
          //         headers: {
          //           Accept: "application/json",
          //           "Content-Type": "application/json",
          //           "X-Requested-With": "XMLHttpRequest"
          //         },
          //         body: JSON.stringify({
          //           username: googleUsername.replace(/ /g, ""),
          //           password: googlePassword
          //         })
          //       })
          //         .then(Response => Response.json())
          //         .then(result => {
          //           console.log(result.data);
          //           console.log(result.data.status);
          //           if (result.data.status === 422) {
          //             const user = JSON.parse(result.data.message);
          //             localStorage.removeItem("token");
          //           } else {
          //             if (result.data.auth_key) {
          //               this.props.changeUserType("Normal");
          //               localStorage.setItem("token", result.data.auth_key);
          //               localStorage.setItem("username", result.data.username);
          //               localStorage.setItem("custId", result.data.custId);
          //               localStorage.setItem("author_id", result.data.id);
          //               window.location.href = "/checkout";
          //             } else {
          //               window.location.href = "/login";
          //             }
          //           }
          //         });
          //     }
          //   } else {
          //     console.log(result.data);
          //     console.log("Success");
          //     this.setState({
          //       registration_success_message: "Registration Successfully!!!"
          //     });
          //     localStorage.setItem("custId", result.data.custId);
          //     localStorage.setItem("username", result.data.username);
          //     localStorage.setItem("author_id", result.data.id);
          //     localStorage.setItem("token", result.data.access_token);
          //     localStorage.setItem(
          //       "msg_token",
          //       this.state.registration_success_message
          //     );
          //     window.location.href = "/checkout";
          //   }
        });
    }
  };

  login(event) {
    fetch(`${API.login}`, {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest",
      },
      body: JSON.stringify({
        email: this.state.Email,
        password: this.state.Password
      })
    }).then(Response => Response.json())
      .then(result => {
        if (result.message) {
          localStorage.removeItem("token");
          this.setState({
            error_email: result.errors.email ? result.errors.email : ""
          });
          this.setState({
            error_password: result.errors.password ? result.errors.password : ""
          });
        } else {
          console.log(result);
          localStorage.setItem("token", result.data.authorization);
          localStorage.setItem("author_id", result.data.id);
          window.location.href = "/checkout";
        }
      });
  }

  //Registrations
  Registration(event) {
    fetch(`${API.register}`, {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: JSON.stringify({
        name: this.state.register_first_name,
        last_name: this.state.register_last_name,
        email: this.state.register_email,
        password: this.state.register_password,
        password_confirmation: this.state.register_c_password,
        terms: 1
      })
    })
      .then(Response => Response.json())
      .then(result => {
        if (result.errors) {
          console.log("Error");
          this.setState({
            register_first_name_error: result.errors.name
              ? result.errors.name
              : ""
          });
          this.setState({
            register_last_name_error: result.errors.last_name
              ? result.errors.last_name
              : ""
          });
          this.setState({
            register_email_error: result.errors.email ? result.errors.email : ""
          });
          this.setState({
            register_password_error: result.errors.password
              ? result.errors.password
              : ""
          });
          this.setState({
            register_phone_error: "The phone field is required."
              ? "The phone field is required."
              : ""
          });
        } else {
          console.log("Success");
          console.log(result);
          localStorage.setItem("token", result.data.authorization);
          window.location.href = "/checkout";
        }
      });
  }

  render() {
    /*  localStorage.removeItem('token');*/
    var tokenCheck = localStorage.getItem("token");
    if (tokenCheck) {
      this.props.history.push("/checkout");
    }
    // const responseGoogle = response => {
    //   console.log(response);
    // };
    return (
      <div>
        <Breadcrumb title={"Login"} />
        {/*Login section*/}
        <section className="login-page section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-lg-4">
                <h3>Sign in</h3>
                <div className="theme-card">
                  <form className="theme-form">
                    <div className="form-group">
                      <label htmlFor="email">
                        Email<span className="error">*</span>
                      </label>
                      <input
                        type="text"
                        onChange={this.Email}
                        className="form-control"
                        id="email"
                        placeholder="Email"
                        required=""
                      />
                      <span className="error">{this.state.error_email}</span>
                    </div>
                    <div className="form-group">
                      <label htmlFor="review">
                        Password<span className="error">*</span>
                      </label>
                      <input
                        type="password"
                        onChange={this.Password}
                        className="form-control"
                        id="review"
                        placeholder="Enter your password"
                        required=""
                      />
                      <span className="error">{this.state.error_password}</span>
                    </div>
                    <a className="btn btn-solid" onClick={this.login}>
                      Login
                    </a>
                  </form>
                  <br />
                  {/* <Facebook /> */}
                  <GoogleLogin
                    clientId="396867180154-fttoimk313ft49uj5iubgiv5jmr89dr4.apps.googleusercontent.com"
                    buttonText="Login with Google"
                    onSuccess={this.responseGoogle}
                    onFailure={this.responseGoogle}
                    cookiePolicy={"single_host_origin"}
                  />
                </div>
              </div>
              <div className="col-lg-8 right-login">
                <h3>No Account? Register</h3>
                <div className="theme-card">
                  <form className="theme-form">
                    <div className="form-row">
                      <div className="col-md-4">
                        <label htmlFor="email">
                          First Name<span className="error">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="fname"
                          placeholder="First Name"
                          required=""
                          name="register_first_name"
                          onChange={this.register_first_name}
                        />
                        <span className="error">
                          {this.state.register_first_name_error}
                        </span>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="review">Last Name</label>
                        <input
                          type="text"
                          className="form-control"
                          id="lname"
                          placeholder="Last Name"
                          required=""
                          name="register_last_name"
                          onChange={this.register_last_name}
                        />
                        <span className="error">
                          {this.state.register_last_name_error}
                        </span>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="email">
                          email<span className="error">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="register_email"
                          placeholder="sometings@gmail.com"
                          required=""
                          name="register_email"
                          onChange={this.register_email}
                        />
                        <span className="error">
                          {this.state.register_email_error}
                        </span>
                      </div>
                    </div>

                    <div className="form-row margin-10">
                      <div className="col-md-4">
                        <label htmlFor="email">Phone</label>
                        <input
                          type="text"
                          className="form-control"
                          id="register_phone"
                          placeholder="Phone Number"
                          required="The Phone field is required."
                          name="register_phone"
                          onChange={this.register_phone}
                        />
                        <span className="error">
                          {this.state.register_phone_error}
                        </span>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="email">
                          Password<span className="error">*</span>
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          id="register_password"
                          placeholder="Enter you password"
                          required=""
                          name="register_password"
                          onChange={this.register_password}
                        />
                        <span className="error">
                          {this.state.register_password_error}
                        </span>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="review">Retype Password</label>
                        <input
                          type="password"
                          className="form-control"
                          id="register_c_password"
                          placeholder="Re-enter your password"
                          required=""
                          name="register_c_password"
                          onChange={this.register_c_password}
                        />
                      </div>
                    </div>
                    <a className="btn btn-solid" onClick={this.Registration}>
                      create Account
                    </a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  userPostFetch: userInfo => dispatch(userPostFetch(userInfo))
});
export default connect(null, mapDispatchToProps)(Login);
