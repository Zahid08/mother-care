import React, {Component} from 'react';
import Breadcrumb from "../common/breadcrumb";
import {Link} from "react-router-dom";
import MyOrders from './my-orders';
import wishList from '../wishlist/index';
import ChangePass from './change-pass';


class DashboardLeft extends Component {

    constructor (props) {
        super (props)
    }

    Logout(event) {
        localStorage.removeItem('token');
        window.location.href='/login'
    }

    render (){
        return (
                <div className="dashboard-left">
                    <div className="collection-mobile-back">
                                    <span className="filter-back">
                                        <i className="fa fa-angle-left" aria-hidden="true"></i> back
                                    </span>
                    </div>
                    <div className="block-content">
                        <ul>
                            <li className="active">
                                <Link to={`${process.env.PUBLIC_URL}/dashboard`}>
                                    Account Info
                                </Link>
                            </li>
                            <li>
                                <Link to={`${process.env.PUBLIC_URL}/my-orders`}>
                                    My Orders
                                </Link>
                            </li>
                            <li>
                                <Link to={`${process.env.PUBLIC_URL}/wishlist`}>
                                    My Wishlist
                                </Link>
                            </li>
                            <li>
                                <Link to={`${process.env.PUBLIC_URL}/change-pass`}>
                                    Change Password
                                </Link>
                            </li>
                            <li className="last"><Link to={'#'} data-lng="en" onClick={this.Logout}>Logout</Link></li>
                        </ul>
                    </div>
                </div>
        )
    }
}

export default DashboardLeft