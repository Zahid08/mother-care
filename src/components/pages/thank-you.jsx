import React, {Component} from 'react';
import Breadcrumb from "../common/breadcrumb";

class ThankYou extends Component {

    constructor (props) {
        super (props)
    }

    render (){


        return (
            <div>
                <Breadcrumb title={'Thank You'}/>


                {/*Forget Password section*/}
                <section className="pwd-page section-b-space">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 offset-lg-3">
                                <h1>Thank You..!!!</h1>
                                <h2>for Buying Our Product</h2>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

export default ThankYou