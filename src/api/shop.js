/**
 * Mocking client-server processing
 */
import _products from './data';
import axios from "axios";


let BASE_URL = 'http://backend.motherscarebd.com'
const API_URL = `${BASE_URL}/api/v1/e-commerce/shop/products-list`;

axios.get(API_URL).then(response =>{
    const serializedStateAPI = JSON.stringify(response.data);
    localStorage.setItem('state_api_data', serializedStateAPI);
}).catch(error => console.log(error));


function loadFromLocalStorage() {
    try {
        const serializedStateAPI = localStorage.getItem('state_api_data')
        if(serializedStateAPI === null) return undefined
        return JSON.parse(serializedStateAPI)
    }catch (e) {
        console.log(e)
        return undefined
    }
}

const persistedStateAPI = loadFromLocalStorage()

/*console.log(persistedStateAPI);
console.log(_products);*/

const TIMEOUT = 100

export default {
    getProducts: (cb, timeout) => setTimeout(() => cb(persistedStateAPI), timeout || TIMEOUT),
    buyProducts: (payload, cb, timeout) => setTimeout(() => cb(), timeout || TIMEOUT)
}
